<?php

class M_collection extends CI_Model
{
	function get_data($id_fish) {
		$data	= $this->db->select('*')
						   ->from ('fish_collection')
						   ->where('id_fish', $id_fish)
						   ->get()
						   ->first_row();
		return $data;
	}
	
	function get_gallery($id_fish) {
		$data	= $this->db->select('*')
						   ->from('fish_gallery')
						   ->where('id_fish', $id_fish)
						   ->get()
						   ->result();
		return $data;
	}
}

?>