<?php

class M_menu_collection extends CI_Model
{
	function get_icon ($id_menu) {
		$data	= $this->db->select("fish_collection.id_fish, fish_collection.menu_icon_id, fish_collection.menu_icon_eng, fish_collection.id_menu, fish_collection.publish")
						   ->from("fish_collection")
						   ->where("fish_collection.id_menu", $id_menu)
						   ->get()
						   ->result();
		
		return $data;
	}
	
	function get_headerMenu ($id_menu) {
		$data	= $this->db->select("header_id_img, header_eng_img")
						   ->from("menu")
						   ->where("id_menu", $id_menu)
						   ->get()
						   ->first_row();
		
		return $data;
	}
}

?>