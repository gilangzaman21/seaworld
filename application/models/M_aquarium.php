<?php

class M_aquarium extends CI_Model
{
	function get_menu ($id_menu) {
		$data	= $this->db->select("*")
						   ->from("menu")
						   ->where("id_menu", $id_menu)
						   ->get()
						   ->first_row();
		
		return $data;
	}
	
	function get_fish ($id_menu) {
		$data	= $this->db->select("*")
						   ->from("fish_collection")
						   ->where("id_menu", $id_menu)
						   ->get()
						   ->result();
		
		return $data;
	}
}

?>