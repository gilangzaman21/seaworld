<?php
class M_dashboardwahana extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	
	function get_data() {
		$data = $this->db->query("SELECT DATE(date_using) AS date, COUNT(DISTINCT id_history) total 
												FROM history_apps GROUP BY DATE(date_using)");
		return $data->result_array();
	}
	
	function get_detail() {
		$data = $this->db->query("SELECT DATE(date_using) AS tgl, DATE_FORMAT(date_using, '%W | %M, %D %Y') AS date, COUNT(DISTINCT id_history) total 
												FROM history_apps GROUP BY DATE(date_using)");
		return $data->result_array();
	}
  
}
?>