<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_player extends CI_Model {

	public $table = 'player';
    public $id = 'nik';
    public $order = 'ASC';
	
    public function __construct() {
        parent::__construct();
    }
    
    function get_data() {
        $data = $this->db->select("id_player, username, email, DATE_FORMAT(birthday, '%d %M %Y') AS birthday, phone, DATE_FORMAT(reg_time, '%d %M %Y | %k:%i:%s') AS reg_time")
						 ->from($this->table)
						 ->order_by('reg_time', 'DESC')
						 ->get()
						 ->result();
		return $data;
    }
	
	function get_by_id($id) {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
	
	function update($data, $id_kategori_sampah){
		$this->db->where('id_kategori_sampah',$id_kategori_sampah);
		$this->db->update('kategori_sampah', $data);
	}
	// delete data
    function delete($id) {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }
}
