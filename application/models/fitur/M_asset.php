<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_asset extends CI_Model {
	
	public $table = 'version';
    public $id = 'id_version';
    public $order = 'DESC';

    public function __construct() {
        parent::__construct();
    }
	
	public function get_data () {
		$data = $this->db->select("id_version, code, whats_new, DATE_FORMAT(update_time, '%d %M %Y | %k:%i:%s') AS update_time")
						 ->from($this->table)
						 ->order_by($this->id, $this->order)
						 ->get()
						 ->result();
		return $data;
	}
}
