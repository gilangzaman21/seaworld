<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_zone extends CI_Model {

	public $table = "master_stage";
    public $id = "id_master_stage";
	
    public function __construct() {
        parent::__construct();
    }
    
    function get_data() {
        $data = $this->db->select("id_master_stage, stage, min_level, unlock_status, DATE_FORMAT(date_opened, '%W, %d %M %Y | %H:%i') AS date_opened")
						 ->from($this->table)
						 ->get()
						 ->result();
		return $data;
    }
	
	function insert($data){
		$insert = $this->db->insert($this->table, $data);
	}
	
	function get_by_id($id) {
        //$this->db->where($this->id, $id);
        //return $this->db->get($this->table)->row();
		return $this->db->select("*, DATE_FORMAT(date_opened, '%W, %d %M %Y | %H:%i') AS date_opened")
						->from($this->table)
						->where($this->id, $id)
						->get()
						->row();
    }
	
	function update($data, $id_zone){
		$this->db->where($this->id, $id_zone);
		$this->db->update($this->table, $data);
	}
	// delete data
    function delete($id) {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }
}