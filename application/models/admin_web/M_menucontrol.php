<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_menucontrol extends CI_Model {

	public $table = "menu";
    public $id = "id_menu";
	
    public function __construct() {
        parent::__construct();
    }
    
    function get_data() {
        $data = $this->db->select("*")
						 ->from($this->table)
						 ->get()
						 ->result();
		return $data;
    }
	
	function insert($data){
		$insert = $this->db->insert($this->table, $data);
	}
	
	function get_by_id($id) {
        //$this->db->where($this->id, $id);
        //return $this->db->get($this->table)->row();
		return $this->db->select("*")
						->from($this->table)
						->where($this->id, $id)
						->get()
						->row();
    }
	
	function update($data, $id_zone){
		$this->db->where($this->id, $id_zone);
		$this->db->update($this->table, $data);
	}
	// delete data
    function delete($id) {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }
}