<?php
//error_reporting(E_ALL ^ E_DEPRECATED);
//error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');
class Get_produk {
	var $db = null;
	function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("login"));
		}
		$this->load->helper('url');
		$this->load->model('Common');
		$autoload['libraries'] = ['session'];
		$this->db = $this->Common->GetDB();
	}
	public function simpan($produk) {
		$api 	= "http://wargamandiri.com/api/cek_harga/pilih/" . $produk;
		$data	= file_get_contents($api);
		$array	= json_decode($data, TRUE);
		
		if ($produk == "pulsa") {
			$bersih	= $this->db->truncate('m_harga_pulsa'); //reset table m_harga_pulsa
			foreach ($array["message"] AS $row) {
				$dataInsert		= array (
										'operator'			=> $row["operator"],
										'code'				=> $row["code"],
										'description'		=> $row["description"],
										'price'				=> $row["price"],
										'harga_jual'		=> $row["price"] + 500,
										'status'			=> $row["status"]
									);
				$insert		= $this->db->insert('m_harga_pulsa', $dataInsert); //Simpan Data Pulsa Terbaru
			}
		} else if ($produk == "pln") {
			$bersih	= $this->db->truncate('m_harga_pln'); //reset table m_harga_pulsa
			foreach ($array["message"] AS $row) {
				$dataInsert		= array (
										'code'				=> $row["code"],
										'description'		=> $row["description"],
										'price'				=> $row["price"],
										'harga_jual'		=> $row["price"] + 500,
										'status'			=> $row["status"]
									);
				$insert		= $this->db->insert('m_harga_pln', $dataInsert); //Simpan Data Pulsa Terbaru
			}
		}
	}

}
