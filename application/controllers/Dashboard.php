<?php
error_reporting(E_ALL ^ E_DEPRECATED);
error_reporting(0);
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	function __construct() 
	{
        parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("login"));
		}
		//$this->load->helper('url');
		$this->load->model('M_dashboard');
    }
	
    public function index() {
		
		$this->template->display('dashboard',$data);
    }

    function logout() {
        $this->session->sess_destroy();
        redirect('login');
    }

}