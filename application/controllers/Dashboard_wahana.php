<?php
//error_reporting(E_ALL ^ E_DEPRECATED);
//error_reporting(0);
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard_wahana extends CI_Controller {
	function __construct() 
	{
        parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("login"));
		}
		$this->load->helper('url');
		$this->load->model('M_dashboardwahana');
    }
	
    public function index() {
		$data['grafik']	= $this->M_dashboardwahana->get_data();
		$this->template->display('dashboard_wahana', $data);
    }
	
	public function detail() {
		$data['grafik']	= $this->M_dashboardwahana->get_detail();
		$this->template->display('detail_grafik', $data);
	}

    function logout() {
        $this->session->sess_destroy();
        redirect('login');
    }

}
