<?php

class Aquarium extends CI_Controller {

    function __construct() {
        parent::__construct();
		
		$this->load->model("M_aquarium");
    }
    
	function id($id_menu) {
		$data["fish_collection"]	= $this->M_aquarium->get_fish($id_menu);
		$data["detail"]				= $this->M_aquarium->get_menu($id_menu);
        
		$this->load->view('aquarium/view_id', $data);
    }
	
	function eng($id_menu) {
		$data["detail"]	= $this->M_aquarium->get_menu($id_menu);
        $this->load->view('aquarium/view_eng', $data);
    }
}
