<?php
error_reporting(E_ALL ^ E_DEPRECATED);
error_reporting(0);
class Menu_admin extends CI_Controller{
    
    function __construct() {
        parent::__construct();
		if($this->session->userdata('status') != "login"){			redirect(base_url("login"));		}
		//$this->load->helper('url');
    }
    
    function index() {       
        $data['record']=  $this->db->get('tb_menu')->result();
        $this->template->display('menu_admin/view',$data);
    }
    
    function add() {
        if(isset($_POST['submit'])) {
            $data   =   array(  'nama_menu' =>  $_POST['nama'],
                                'link'      =>  $_POST['link'],
                                'icon'      =>  $_POST['icon'],
								'user_role' =>  $_POST['user_role'],
                                'kat_menu'  =>  $_POST['kat_menu']);
            $this->db->insert('tb_menu',$data);
            redirect('menu_admin');
        }
        else {
            $data['record']=$this->db->get_where('tb_menu', array('kat_menu' =>0))->result();            
            $this->template->display('menu_admin/tambah',$data);
        }
    }
    
    function edit()
    {
        if(isset($_POST['submit']))
        {
            $data   =   array(  'nama_menu' =>  $_POST['nama'],
                                'link'      =>  $_POST['link'],
                                'icon'      =>  $_POST['icon'],
								'user_role' =>  $_POST['user_role'],
                                'kat_menu'  =>  $_POST['kat_menu']);
            $this->db->where('id_menu',$_POST['id']);
            $this->db->update('tb_menu',$data);
            redirect('menu_admin');
        }
        else {
            $id= $this->uri->segment(3);
            $data['record']=  $this->db->get_where('tb_menu',array('id_menu'=> $id))->row_array();
            $data['katmenu']=$this->db->get_where('tb_menu', array('kat_menu' =>0))->result(); 
            $this->template->display('menu_admin/edit',$data);
        }
    }
    
    function delete($id){
		$this->db->where('id_menu',$id);
		$this->db->delete('tb_menu');
       	redirect('menu_admin');
    }
}