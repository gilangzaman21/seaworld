<?php
//error_reporting(E_ALL ^ E_DEPRECATED);
//error_reporting(0);

defined('BASEPATH') OR exit('No direct script access allowed');
class Asset extends CI_Controller {
    function __construct() 
	{
        parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("login"));
		}
		//$this->load->helper('url');
		$this->load->model('fitur/M_asset');
		$this->load->library('datatables');
    }
    public function index()
    {
		$data['version'] = $this->M_asset->get_data();
    	$this->template->display('fitur/asset/view', $data);
    }
}
?>
