<?php
//error_reporting(E_ALL ^ E_DEPRECATED);
//error_reporting(0);

defined('BASEPATH') OR exit('No direct script access allowed');
class Zone extends CI_Controller {
    function __construct() 
	{
        parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("login"));
		}
		//$this->load->helper('url');
		$this->load->model('fitur/M_zone');
		$this->load->library('datatables');
    }
    public function index()
    {
		$data['zone'] = $this->M_zone->get_data();
    	$this->template->display('fitur/zone/view', $data);
    }
	
	function insert() {
		$this->template->display('fitur/zone/insert');
	}
	
	function detail($id) {
		$row				= $this->M_zone->get_by_id($id);
		
		if ($row) {
			$data		= array(
											"id_zone"			=> $row->id_master_stage,
											"zone"				=> $row->stage,
											"min_level"		=> $row->min_level,
											"unlock_status"	=> $row->unlock_status,
											"date_opened"	=> $row->date_opened,
										);
		}
		$this->template->display('fitur/zone/detail',$data);
		
	}
	
	function edit($id) {
		$row = $this->M_zone->get_by_id($id);
        if ($row) {
            $data		= array(
											"id_zone"			=> $row->id_master_stage,
											"zone"				=> $row->stage,
											"min_level"		=> $row->min_level,
											"unlock_status"	=> $row->unlock_status,
											"date_opened"	=> $row->date_opened,
										);
			$this->template->display('fitur/zone/edit',$data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('fitur/zone'));
        }
    }
	
	function input_proses() {
		$id_zone			= $this->security->xss_clean($this->input->post("id_zone"));
		$zone				= $this->security->xss_clean($this->input->post("zone"));
		$min_level		= $this->security->xss_clean($this->input->post("min_level"));
		$unlock_status	= $this->security->xss_clean($this->input->post("unlock_status"));
		$date_opened	= $this->security->xss_clean($this->input->post("date_opened"));
		
		if($id_zone != NULL) {
			$data_update	= array (
										"id_master_stage"	=> $id_zone,
										"stage"					=> $zone,
										"min_level"			=> $min_level,
										"unlock_status"		=> $unlock_status,
										"date_opened"		=> date("Y-m-d H:i:s", strtotime($date_opened) )
									);
						
			$update	= $this->M_zone->update($data_update, $id_zone);
			redirect(site_url('fitur/zone'));
		} else {
			$data_insert		= array (
										"stage"				=> $zone,
										"min_level"		=> $min_level,
										"unlock_status"	=> $unlock_status,
										"date_opened"	=> date("Y-m-d H:i:s", strtotime($date_opened) )
									);
									
			$insert		= $this->M_zone->insert($data_insert);
			redirect(site_url('fitur/zone'));
		}
	}
	
	function delete($id) 
    {
        $row = $this->M_zone->get_by_id($id);

        if ($row) {
            $this->M_zone->delete($id);
            //$this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('fitur/zone'));
        } else {
            //$this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('fitur/zone'));
        }
    }
}
?>
