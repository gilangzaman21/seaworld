<?php
//error_reporting(E_ALL ^ E_DEPRECATED);
//error_reporting(0);

defined('BASEPATH') OR exit('No direct script access allowed');
class Player extends CI_Controller {
    function __construct() 
	{
        parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("login"));
		}
		//$this->load->helper('url');
		$this->load->model('fitur/M_player');
		$this->load->library('datatables');
    }
    public function index()
    {
		$data['player'] = $this->M_player->get_data();
		//echo json_encode($data['player']);
    	$this->template->display('fitur/player/view', $data);
    }
	
	public function edit($id) {
		$row = $this->M_player->get_by_id($id);
        if ($row) {
            $data = array(
			'id_player' 	=> $row->id_player,
			'username' 	=> $row->username,
			'email' 			=> $row->email,
			'birthday' 		=> $row->birthday,
			'phone'		 	=> $row->phone,
			'reg_time'		=> $row->reg_time
	    );
			$this->template->display('fitur/player/edit',$data);
            //$this->load->view('user/user_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('fitur/player'));
        }
    }
}
?>
