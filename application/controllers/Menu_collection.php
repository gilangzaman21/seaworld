<?php

class Menu_collection extends CI_Controller {

    function __construct() {
        parent::__construct();
		
		$this->load->model("M_menu_collection");
    }
    
	function id($id_menu) {
		$data["icon_collection"]	= $this->M_menu_collection->get_icon($id_menu);
		$data["header_menu"]		= $this->M_menu_collection->get_headerMenu($id_menu);
        $this->load->view('menu_collection/view_id', $data);
    }
	
	function eng($id_menu) {
		$data["icon_collection"]	= $this->M_menu_collection->get_icon($id_menu);
		$data["header_menu"]		= $this->M_menu_collection->get_headerMenu($id_menu);
        $this->load->view('menu_collection/view_eng', $data);
    }
}
