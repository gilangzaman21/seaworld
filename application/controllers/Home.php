<?php

class Home extends CI_Controller {

    function __construct() {
        parent::__construct();
		
		$this->load->model("M_home");
    }
    function id() {
        $this->load->view('home/view_id');
    }
	
	function eng() {
        $this->load->view('home/view_eng');
    }
}
