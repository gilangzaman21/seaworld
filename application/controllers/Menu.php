<?php

class Menu extends CI_Controller {

    function __construct() {
        parent::__construct();
		
		$this->load->model("M_menu");
    }
    function id() {
		$data["menu"]	= $this->M_menu->get_menu();
        $this->load->view('menu/view_id', $data);
    }
	
	function eng() {
		$data["menu"]	= $this->M_menu->get_menu();
        $this->load->view('menu/view_eng', $data);
    }
	
	function error() {
		$this->load->view('menu/view_error');
	}
	
	/*##### Menu Jadwal Feeding Show #####*/
	function jadwal_id() {
		$this->load->view('home/menu_jadwalFeedingShow/view_id');
	}
	
	function jadwal_eng() {
		$this->load->view('home/menu_jadwalFeedingShow/view_eng');
	}
	/*##### END Menu Jadwal Feeding Show #####*/
	
	/*##### Menu Program Edukasi #####*/
	function edukasi_id() {
		$this->load->view('home/menu_programEdukasi/view_id');
	}
	
	function edukasi_eng() {
		$this->load->view('home/menu_programEdukasi/view_eng');
	}
	/*##### END Menu Program Edukasi #####*/
	
	/*##### Menu Event #####*/
	function event_id() {
		$this->load->view('home/menu_event/view_id');
	}
	
	function event_eng() {
		$this->load->view('home/menu_event/view_eng');
	}
	/*##### END Menu Event #####*/
	
	/*##### Menu FunDive #####*/
	function FunDive_id() {
		$this->load->view('home/menu_FunDive/view_id');
	}
	
	function FunDive_eng() {
		$this->load->view('home/menu_FunDive/view_eng');
	}
	/*##### END Menu FunDive #####*/
	
	/*##### Menu Marketing #####*/
	function marketing_id() {
		$this->load->view('home/menu_marketing/view_id');
	}
	
	function marketing_eng() {
		$this->load->view('home/menu_marketing/view_eng');
	}
	/*##### END Menu Marketing #####*/
}
