<?php
error_reporting(E_ALL ^ E_DEPRECATED);
error_reporting(0);
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("login"));
		}
		//$this->load->helper('url');
        $this->load->model('User_model');
        //$this->load->library('form_validation');
    }

    public function index()
    {
		//$this->template->display('user/user_list',$data);
        $user = $this->User_model->get_all();

        $data = array(
            'user_data' => $user
        );

        $this->template->display('user/user_list-new',$data);
		//$this->load->view('user/user_list', $data);
    }

    public function read($id) 
    {
        $row = $this->User_model->get_by_id($id);
        if ($row) {
            $data = array(
		'u_id' => $row->u_id,
		'nama' => $row->nama,
		'u_name' => $row->u_name,
		'u_paswd' => $row->u_paswd,
		'u_foto' => $row->u_foto,
		'role' => $row->role,
	    );
			$this->template->display('user/user_read',$data);
            //$this->load->view('user/user_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('user'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('user/create_action'),
	    'u_id' => set_value('u_id'),
	    'nama' => set_value('nama'),
	    'u_name' => set_value('u_name'),
	    'u_paswd' => set_value('u_paswd'),
	    'u_foto' => set_value('u_foto'),
	    'role' => set_value('role'),
	);
        $this->template->display('user/user_form',$data);
		//$this->load->view('user/user_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama' => $this->input->post('nama',TRUE),
		'u_name' => $this->input->post('u_name',TRUE),
		'u_paswd' => MD5($this->input->post('u_paswd',TRUE)),
		'u_foto' => $this->input->post('u_foto',TRUE),
		'role' => $this->input->post('role',TRUE),
	    );

            $this->User_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('user'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->User_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('user/update_action'),
		'u_id' => set_value('u_id', $row->u_id),
		'nama' => set_value('nama', $row->nama),
		'u_name' => set_value('u_name', $row->u_name),
		'u_paswd' => set_value('u_paswd', $row->u_paswd),
		'u_foto' => set_value('u_foto', $row->u_foto),
		'role' => set_value('role', $row->role),
	    );
            $this->template->display('user/user_form',$data);
			//$this->load->view('user/user_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('user'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('u_id', TRUE));
        } else {
            $data = array(
		'nama' => $this->input->post('nama',TRUE),
		'u_name' => $this->input->post('u_name',TRUE),
		'u_paswd' => MD5($this->input->post('u_paswd',TRUE)),
		'u_foto' => $this->input->post('u_foto',TRUE),
		'role' => $this->input->post('role',TRUE),
	    );

            $this->User_model->update($this->input->post('u_id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('user'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->User_model->get_by_id($id);

        if ($row) {
            $this->User_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('user'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('user'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nama', 'nama', 'trim|required');
	$this->form_validation->set_rules('u_name', 'u name', 'trim|required');
	$this->form_validation->set_rules('u_paswd', 'u paswd', 'trim|required');
	$this->form_validation->set_rules('u_foto', 'u foto', 'trim|required');
	$this->form_validation->set_rules('role', 'role', 'trim|required');

	$this->form_validation->set_rules('u_id', 'u_id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "user.xls";
        $judul = "user";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Nama");
	xlsWriteLabel($tablehead, $kolomhead++, "U Name");
	xlsWriteLabel($tablehead, $kolomhead++, "U Paswd");
	xlsWriteLabel($tablehead, $kolomhead++, "U Foto");
	xlsWriteLabel($tablehead, $kolomhead++, "Role");

	foreach ($this->User_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->nama);
	    xlsWriteLabel($tablebody, $kolombody++, $data->u_name);
	    xlsWriteLabel($tablebody, $kolombody++, $data->u_paswd);
	    xlsWriteLabel($tablebody, $kolombody++, $data->u_foto);
	    xlsWriteLabel($tablebody, $kolombody++, $data->role);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=user.doc");

        $data = array(
            'user_data' => $this->User_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('user/user_doc',$data);
    }

}

/* End of file User.php */
/* Location: ./application/controllers/User.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-07-12 19:06:20 */
/* http://harviacode.com */