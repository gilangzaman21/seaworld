<?php
//error_reporting(E_ALL ^ E_DEPRECATED);
//error_reporting(0);

defined('BASEPATH') OR exit('No direct script access allowed');
class Fish_control extends CI_Controller {
    function __construct() 
	{
        parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("login"));
		}
		//$this->load->helper('url');
		$this->load->model('admin_web/M_fishcontrol');
		$this->load->library(array('datatables', 'image_lib'));
		//$this->load->helper(array('form', 'url'));
    }
    public function index()
    {
		$data['fish'] = $this->M_fishcontrol->get_data();
    	$this->template->display('admin_web/fish_control/view', $data);
    }
	
	function insert() {
		$this->template->display('admin_web/fish_control/insert');
	}
	
	function detail($id) {
		$row				= $this->M_fishcontrol->get_by_id($id);
		
		if ($row) {
			$data		= array(
											"id_zone"			=> $row->id_master_stage,
											"zone"				=> $row->stage,
											"min_level"			=> $row->min_level,
											"unlock_status"		=> $row->unlock_status,
											"date_opened"		=> $row->date_opened,
										);
		}
		$this->template->display('admin_web/fish_control/detail',$data);
		
	}
	
	function edit($id) {
		$row = $this->M_fishcontrol->get_by_id($id);
        if ($row) {
            $data		= array(
										"id_fish"					=> $row->id_fish,
										"avatar"					=> $row->avatar,
										"headline_id"				=> $row->headline_id,
										"headline_english"			=> $row->headline_english,
										"nama"						=> $row->nama,
										"name"						=> $row->name,
										"info_bahasa"				=> $row->info_bahasa,
										"info_english"				=> $row->info_english,
										"peta_pesebaran"			=> $row->peta_pesebaran,
										"video"						=> $row->video,
										"kelas_klasifikasi"			=> $row->kelas_klasifikasi,
										"ordo_klasifikasi"			=> $row->ordo_klasifikasi,
										"famili_klasifikasi"		=> $row->famili_klasifikasi,
										"marga_klasifikasi"			=> $row->marga_klasifikasi,
										"jenis_klasifikasi"			=> $row->jenis_klasifikasi,
										"makanan"					=> $row->makanan,
										"food"						=> $row->food,
										"panjang_max"				=> $row->panjang_max
										);
			$this->template->display('admin_web/fish_control/edit',$data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin_web/fish_control'));
        }
    }
	
	function input_proses() {
		$id_fish			= $this->security->xss_clean($this->input->post("id_fish"));
		$img_icon			= $this->security->xss_clean($this->input->post("img_icon"));
		$img_peta			= $this->security->xss_clean($this->input->post("img_peta"));
		$video				= $this->security->xss_clean($this->input->post("video"));
		$panjang_maks		= $this->security->xss_clean($this->input->post("panjang_maks"));
		$kelas				= $this->security->xss_clean($this->input->post("kelas"));
		$ordo				= $this->security->xss_clean($this->input->post("ordo"));
		$keluarga			= $this->security->xss_clean($this->input->post("keluarga"));
		$marga				= $this->security->xss_clean($this->input->post("marga"));
		$jenis				= $this->security->xss_clean($this->input->post("jenis"));
		$img_headline_id	= $this->security->xss_clean($this->input->post("img_headline_id"));
		$nama				= $this->security->xss_clean($this->input->post("nama"));
		$informasi			= $this->security->xss_clean($this->input->post("informasi"));
		$makanan			= $this->security->xss_clean($this->input->post("makanan"));
		$img_headline_eng	= $this->security->xss_clean($this->input->post("img_headline_eng"));
		$name				= $this->security->xss_clean($this->input->post("name"));
		$information		= $this->security->xss_clean($this->input->post("information"));
		$food				= $this->security->xss_clean($this->input->post("food"));
		
		//########Get Last ID
		$get_last_id		= $this->M_fishcontrol->getLastID();
		$last_id			= $get_last_id->id_fish + 1;
		//########Get Last ID
		
		//########Upload Avatar
		$name_avatar		= "avatar_" . $last_id . ".png";
		$path_avatar		= "./assets/img/avatar/";
		$url_avatar			= "assets/img/avatar/";
		$avatar_link		= $url_avatar . $name_avatar;
		$upload_avatar		= $this->UploadImage($img_icon, $name_avatar, $path_avatar, $url_avatar);
		//echo json_encode($img_icon);
		//die();
		//########End Upload Avatar
		
		if($id_fish != NULL) {
			$data_update	= array (
										"id_fish"					=> $id_fish,
										"avatar"					=> $avatar_link,
										"headline_id"				=> "test.png",//$headline_id_link,
										"headline_english"			=> "test.png",//$headline_eng_link,
										"nama"						=> $nama,
										"name"						=> $name,
										"info_bahasa"				=> $informasi,
										"info_english"				=> $information,
										"peta_pesebaran"			=> "test.png",//$peta_pesebaran_link,
										"video"						=> "test.png",//$video_link,
										"kelas_klasifikasi"			=> $kelas,
										"ordo_klasifikasi"			=> $ordo,
										"famili_klasifikasi"		=> $keluarga,
										"marga_klasifikasi"			=> $marga,
										"jenis_klasifikasi"			=> $jenis,
										"makanan"					=> $makanan,
										"food"						=> $food,
										"panjang_max"				=> $panjang_maks . " cm"
									);
						
			$update	= $this->M_fishcontrol->update($data_update, $id_fish);
			redirect(site_url('admin_web/fish_control'));
		} else {
			$data_insert		= array (
										"avatar"					=> $avatar_link,
										"headline_id"				=> "test.png",//$headline_id_link,
										"headline_english"			=> "test.png",//$headline_eng_link,
										"nama"						=> $nama,
										"name"						=> $name,
										"info_bahasa"				=> $informasi,
										"info_english"				=> $information,
										"peta_pesebaran"			=> "test.png",//$peta_pesebaran_link,
										"video"						=> "test.png",//$video_link,
										"kelas_klasifikasi"			=> $kelas,
										"ordo_klasifikasi"			=> $ordo,
										"famili_klasifikasi"		=> $keluarga,
										"marga_klasifikasi"			=> $marga,
										"jenis_klasifikasi"			=> $jenis,
										"makanan"					=> $makanan,
										"food"						=> $food,
										"panjang_max"				=> $panjang_maks . " cm"
									);
									
			$insert		= $this->M_fishcontrol->insert($data_insert);
			redirect(site_url('admin_web/fish_control'));
		}
	}
	
	function delete($id) 
    {
        $row = $this->M_fishcontrol->get_by_id($id);

        if ($row) {
            $this->M_fishcontrol->delete($id);
            //$this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('admin_web/fish_control'));
        } else {
            //$this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin_web/fish_control'));
        }
    }
	
	//Upload image ke server
	function uploadImage($image, $name, $path, $url) {
		$config 		= array(
							"allowed_types"		=>	"png",
							"upload_path"		=>	"./assets/img/avatar/",
							"upload_url"		=> 	base_url() . "assets/img/avatar/",
							"file_name"		 	=>	$name, 
							"max_size"			=>	1500);
		
		//memasukkan konfigurasi
		$this->load->library("upload", $config);
		
		
		//Proses upload Image Master
		if ($this->upload->do_upload("img_icon")) {
			//If image upload in folder, set also this value in "$image_data".
			$image_data = $this->upload->data();
		}
		
		return $image_data;
	}
}
?>
