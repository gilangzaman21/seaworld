<?php
//error_reporting(E_ALL ^ E_DEPRECATED);
//error_reporting(0);

defined('BASEPATH') OR exit('No direct script access allowed');
class Menu_control extends CI_Controller {
    function __construct() 
	{
        parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("login"));
		}
		//$this->load->helper('url');
		$this->load->model('admin_web/M_menucontrol');
		$this->load->library('datatables');
    }
    public function index()
    {
		$data['menu'] = $this->M_menucontrol->get_data();
    	$this->template->display('admin_web/menu_control/view', $data);
    }
	
	function insert() {
		$this->template->display('admin_web/menu_control/insert');
	}
	
	function detail($id) {
		$row				= $this->M_fishcontrol->get_by_id($id);
		
		if ($row) {
			$data		= array(
											"id_zone"			=> $row->id_master_stage,
											"zone"				=> $row->stage,
											"min_level"		=> $row->min_level,
											"unlock_status"	=> $row->unlock_status,
											"date_opened"	=> $row->date_opened,
										);
		}
		$this->template->display('admin_web/fish_control/detail',$data);
		
	}
	
	function edit($id) {
		$row = $this->M_menucontrol->get_by_id($id);
        if ($row) {
            $data		= array(
											"id_menu"		=> $row->id_menu,
											"nama"			=> $row->nama,
											"informasi"		=> $row->info_id,
											"name"			=> $row->name,
											"information"	=> $row->info_eng,
										);
			$this->template->display('admin_web/menu_control/edit',$data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin_web/menu_control'));
        }
    }
	
	function input_proses() {
		$id_menu			= $this->security->xss_clean($this->input->post("id_menu"));
		$nama				= $this->security->xss_clean($this->input->post("nama"));
		$informasi			= $this->security->xss_clean($this->input->post("informasi"));
		$name				= $this->security->xss_clean($this->input->post("name"));
		$information		= $this->security->xss_clean($this->input->post("information"));
		
		if($id_menu != NULL) {
			$data_update	= array (
										"id_menu"			=> $id_menu,
										"nama"				=> $nama,
										"info_id"			=> $informasi,
										"name"				=> $name,
										"info_eng"			=> $information
									);
						
			$update	= $this->M_menucontrol->update($data_update, $id_menu);
			redirect(site_url('admin_web/menu_control'));
		} else {
			$data_insert		= array (
										"nama"				=> $nama,
										"info_id"			=> $informasi,
										"name"				=> $name,
										"info_eng"		=> $information
									);
									
			$insert		= $this->M_menucontrol->insert($data_insert);
			redirect(site_url('admin_web/menu_control'));
		}
	}
	
	function delete($id) 
    {
        $row = $this->M_menucontrol->get_by_id($id);

        if ($row) {
            $this->M_menucontrol->delete($id);
            //$this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('admin_web/menu_control'));
        } else {
            //$this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin_web/menu_control'));
        }
    }
}
?>
