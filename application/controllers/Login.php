<?php
error_reporting(E_ALL ^ E_DEPRECATED);
error_reporting(0);
class Login extends CI_Controller {
    function __construct() {        
	parent::__construct();
        $this->load->model(array('M_user'));
        if ($this->session->userdata('u_name')) {
            redirect('Dashboard');
        }
    }
    function index() {
        $this->load->view('login');
    }
    function proses() {
        $this->form_validation->set_rules('username', 'username', 'required|trim');
        $this->form_validation->set_rules('password', 'password', 'required|trim');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('login');
        } else {
            $usr = $this->input->post('username');
            $psw = $this->input->post('password');
            $u = $usr;
            $p = md5($psw);
            $cek = $this->M_user->cek($u, $p);
            if ($cek->num_rows() > 0) {
                //login berhasil, buat session
                foreach ($cek->result() as $qad) {
                    $sess_data['status'] = "login";
					$sess_data['u_id'] = $qad->u_id;
                    $sess_data['id_m_keluarga'] = $qad->id_warga;
					$sess_data['nama'] = $qad->nama;
                    $sess_data['u_name'] = $qad->u_name;
					$sess_data['user_role'] = $qad->user_role;
                    $sess_data['role'] = $qad->role;
                    $this->session->set_userdata($sess_data);
                }
				if ($sess_data['role'] == "Warga") {
					redirect('dashboard_wahana');
				} else if ($sess_data['role'] == "Petugas Bank Sampah") {
					redirect('bank_sampah/dashboard');
				} else if ($sess_data['role'] == "Petugas Posyandu") {
					redirect('posyandu/dashboard');
				} else {
					redirect('dashboard');
				}               
            } else {
                $this->session->set_flashdata('result_login', '<br>Username atau Password yang anda masukkan salah.');
                redirect('login');
            }
        }
    }
    public function logout() {
        $this->session->unset_userdata('nama');
		$this->session->unset_userdata('role');
		$this->session->unset_userdata('status');
		session_destroy();
		redirect('login');
    }
}
