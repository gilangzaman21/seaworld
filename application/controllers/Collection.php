<?php

class Collection extends CI_Controller {

    function __construct() {
        parent::__construct();
		
		$this->load->model("M_collection");
    }
    
	function id($id_fish) {
		//Get Fish
		$get_fish		= $this->M_collection->get_data($id_fish);
		
		//Get Gallery
		$get_gallery 	= $this->M_collection->get_gallery($id_fish);
		
		$data["detail"] 		= array (
										'id_fish'				=> $get_fish->id_fish,
										'id_menu'				=> $get_fish->id_menu,
										'avatar'				=> $get_fish->avatar,
										'headline_id'			=> $get_fish->headline_id,
										'info_bahasa'			=> $get_fish->info_bahasa,
										'peta'					=> $get_fish->peta_pesebaran,
										'video'					=> $get_fish->video,
										'kelas'					=> $get_fish->kelas_klasifikasi,
										'ordo'					=> $get_fish->ordo_klasifikasi,
										'famili'				=> $get_fish->famili_klasifikasi,
										'marga'					=> $get_fish->marga_klasifikasi,
										'jenis'					=> $get_fish->jenis_klasifikasi,
										'makanan'				=> $get_fish->makanan,
										'panjang'				=> $get_fish->panjang_max,
										'status_konservasi'		=> $get_fish->status_konservasi
									);
		
		$data["gallery"]		= $get_gallery;
		
        $this->load->view('collection/view_id', $data);
    }
	
	function eng($id_fish) {
		//Get Fish
		$get_fish		= $this->M_collection->get_data($id_fish);
		
		//Get Gallery
		$get_gallery 	= $this->M_collection->get_gallery($id_fish);
		
		$data["detail"] 		= array (
										'id_fish'				=> $get_fish->id_fish,
										'id_menu'				=> $get_fish->id_menu,
										'avatar'				=> $get_fish->avatar,
										'headline_english'		=> $get_fish->headline_english,
										'info_english'			=> $get_fish->info_english,
										'peta'					=> $get_fish->peta_pesebaran,
										'video'					=> $get_fish->video,
										'kelas'					=> $get_fish->kelas_klasifikasi,
										'ordo'					=> $get_fish->ordo_klasifikasi,
										'famili'				=> $get_fish->famili_klasifikasi,
										'marga'					=> $get_fish->marga_klasifikasi,
										'jenis'					=> $get_fish->jenis_klasifikasi,
										'food'					=> $get_fish->food,
										'panjang'				=> $get_fish->panjang_max,
										'conservation_status'	=> $get_fish->conservation_status
									);
		
		$data["gallery"]		= $get_gallery;
		
        $this->load->view('collection/view_eng', $data);
    }
	
	function error($id_fish) {
		//Get Fish
		$get_fish		= $this->M_collection->get_data($id_fish);
		
		//Get Gallery
		$get_gallery 	= $this->M_collection->get_gallery($id_fish);
		
		$data["detail"] 		= array (
										'id_fish'				=> $get_fish->id_fish,
										'id_menu'				=> $get_fish->id_menu,
										'avatar'				=> $get_fish->avatar,
										'headline_english'		=> $get_fish->headline_english,
										'info_english'			=> $get_fish->info_english,
										'peta'					=> $get_fish->peta_pesebaran,
										'video'					=> $get_fish->video,
										'kelas'					=> $get_fish->kelas_klasifikasi,
										'ordo'					=> $get_fish->ordo_klasifikasi,
										'famili'				=> $get_fish->famili_klasifikasi,
										'marga'					=> $get_fish->marga_klasifikasi,
										'jenis'					=> $get_fish->jenis_klasifikasi,
										'food'					=> $get_fish->food,
										'panjang'				=> $get_fish->panjang_max,
										'conservation_status'	=> $get_fish->conservation_status
									);
		
		$data["gallery"]		= $get_gallery;
		
		$this->load->view('collection/view_error', $data);
	}
}
