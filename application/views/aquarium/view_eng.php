<!DOCTYPE html>
<html lang="id">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>SeaWorld</title>
  <!-- Favicon-->
    <link rel="icon" href="img/favicon.png" type="image/x-icon">
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets/img/logo.png'); ?>">

  <!-- CSS -->
	<link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet" >
	<link href="<?php echo base_url('assets/css/style_aquarium.css'); ?>" rel="stylesheet" >
</head>

<body onload="setInterval('displayServerTime()', 1000);">

	<audio src="<?php echo base_url('assets/img/audio/backsound.mp3'); ?>" autoplay="autoplay" loop></audio>
	
	<section id="main">
		<!--ROW HEADER & LOGO-->
		<div class="row">
				<div id="header">
					<b><h3 id="date"></h3></b>
				</div>
				<a href="<?php echo base_url("menu/eng/"); ?>"><input class="btn_home" type="image" src="<?php echo base_url('assets/img/32inch-btn_Home.png'); ?>" onClick="clicksound.playclip()" /></a>
				<div class="logo">
					<img src="<?php echo base_url('assets/img/logo.png'); ?>" class="logo_image" alt="" />
				</div>
		</div>
		
		<!-- ROW MAP & INFO-->
		<div class="row">
			<div class="map">
				<img src="<?php echo base_url($detail->map_location); ?>" class="" alt="" />
			</div>
			<div class="window_info">
				<p><?php echo $detail->info_eng; ?>
				</p>
			</div>
		</div>
		<!-- ROW FRAME -->
		<div class="row">
			<div class="frame">
				<img src="<?php echo base_url($detail->home_image); ?>" class="" alt="" />
			</div>
		</div>
		<!-- ROW FRAME -->
		<div class="row">
			<div class="gallery">
				<?php 
					//foreach ($fish_collection as $collect) {
				?>
				<!--
				<div class="img_gallery">
					<img src="<?php echo base_url($collect->collection_id); ?>" alt="Trolltunga Norway" width="211px" height="186px">
				</div>
				-->
				<div class="img_gallery">
					<img src="<?php echo base_url("assets/img/aquarium/collection_id/sample.png"); ?>" alt="Trolltunga Norway" width="211px" height="186px">
				</div>
				<div class="img_gallery">
					<img src="<?php echo base_url("assets/img/aquarium/collection_id/sample.png"); ?>" alt="Trolltunga Norway" width="211px" height="186px">
				</div>
				<div class="img_gallery">
					<img src="<?php echo base_url("assets/img/aquarium/collection_id/sample.png"); ?>" alt="Trolltunga Norway" width="211px" height="186px">
				</div>
				<div class="img_gallery">
					<img src="<?php echo base_url("assets/img/aquarium/collection_id/sample.png"); ?>" alt="Trolltunga Norway" width="211px" height="186px">
				</div>
				<div class="img_gallery">
					<img src="<?php echo base_url("assets/img/aquarium/collection_id/sample.png"); ?>" alt="Trolltunga Norway" width="211px" height="186px">
				</div>
				<div class="img_gallery">
					<img src="<?php echo base_url("assets/img/aquarium/collection_id/sample.png"); ?>" alt="Trolltunga Norway" width="211px" height="186px">
				</div>
				<?php
					//}
				?>
			</div>	
		</div>
	</section>
	
	<section id="footer">
		<?php 
			$link	= $this->uri->segment('3');
		?>
		<a href="../id/<?php echo $link; ?>"><input id="btn_bahasa" type="image" src="<?php echo base_url('assets/img/btn_indonesia.png'); ?>" onClick="clicksound.playclip()" /></a>
		<a href="../eng/<?php echo $link; ?>"><input id="btn_bahasa" type="image" src="<?php echo base_url('assets/img/btn_english.png'); ?>" onClick="clicksound.playclip()" /></a>
		<h3 id="clock_down"></h3>
		<h3 class="font_bawah">START FROM HERE - THINK FISH</h3>
		<!--
		<button class="popup_feeding" id="btn_popupFeeding" onClick="clicksound.playclip()" data-toggle="modal" data-target="#popupFeeding"></button>
		-->
	</section>
</body>

</html>

	<!-- jQuery 2.1.3 -->
    <script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>	
    <!-- Bootstrap 3.3.2 JS -->
    <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>

<!-- Function DateTime -->	
<script type="text/javascript">
    //set timezone
    <?php date_default_timezone_set('Asia/Jakarta'); ?>
    //buat object date berdasarkan waktu di server
    var serverTime = new Date(<?php print date('Y, m, d, H, i, s, 0'); ?>);
    //buat object date berdasarkan waktu di client
    var clientTime = new Date();
    //hitung selisih
    var Diff = serverTime.getTime() - clientTime.getTime();    
    //fungsi displayTime yang dipanggil di bodyOnLoad dieksekusi tiap 1000ms = 1detik
    function displayServerTime(){
        //buat object date berdasarkan waktu di client
        var clientTime = new Date();
        //buat object date dengan menghitung selisih waktu client dan server
        var time = new Date(clientTime.getTime() + Diff);
        //ambil nilai jam
        var sh = time.getHours().toString();
        //ambil nilai menit
        var sm = time.getMinutes().toString();
        //ambil nilai detik
        var ss = time.getSeconds().toString();
        //tampilkan jam:menit:detik dengan menambahkan angka 0 jika angkanya cuma satu digit (0-9)
        
		var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
		var myDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
		var date = new Date();
		var day = date.getDate();
		var month = date.getMonth();
		var thisDay = date.getDay(),
			thisDay = myDays[thisDay];
		var yy = date.getYear();
		var year = (yy < 1000) ? yy + 1900 : yy;
		var date_now = (thisDay + ', ' + months[month] + ' ' + day + ', ' + year);
	
	//document.getElementById("clock_up").innerHTML = (sh.length==1?"0"+sh:sh) + ":" + (sm.length==1?"0"+sm:sm);
	document.getElementById("clock_down").innerHTML = (sh.length==1?"0"+sh:sh) + ":" + (sm.length==1?"0"+sm:sm);
	document.getElementById("date").innerHTML = date_now;
	document.getElementById("date").innerHTML = date_now;
    }
</script>
<!-- END Function DateTime -->


<!-- Function Sound Effect Click -->
<script>
// Mouseover/ Click sound effect- by JavaScript Kit (www.javascriptkit.com)
// Visit JavaScript Kit at http://www.javascriptkit.com/ for full source code

//** Usage: Instantiate script by calling: var uniquevar=createsoundbite("soundfile1", "fallbackfile2", "fallebacksound3", etc)
//** Call: uniquevar.playclip() to play sound

var html5_audiotypes={ //define list of audio file extensions and their associated audio types. Add to it if your specified audio file isn't on this list:
	"mp3": "audio/mpeg",
	"mp4": "audio/mp4",
	"ogg": "audio/ogg",
	"wav": "audio/wav"
}

function createsoundbite(sound){
	var html5audio=document.createElement('audio')
	if (html5audio.canPlayType){ //check support for HTML5 audio
		for (var i=0; i<arguments.length; i++){
			var sourceel=document.createElement('source')
			sourceel.setAttribute('src', arguments[i])
			if (arguments[i].match(/\.(\w+)$/i))
				sourceel.setAttribute('type', html5_audiotypes[RegExp.$1])
			html5audio.appendChild(sourceel)
		}
		html5audio.load()
		html5audio.playclip=function(){
			html5audio.pause()
			html5audio.currentTime=0
			html5audio.play()
		}
		return html5audio
	}
	else{
		return {playclip:function(){throw new Error("Your browser doesn't support HTML5 audio unfortunately")}}
	}
}

//Initialize two sound clips with 1 fallback file each:

//var mouseoversound=createsoundbite("whistle.ogg", "whistle.mp3")
var clicksound=createsoundbite("../assets/img/audio/btn_click.mp3")
</script>
<!-- END Function Sound Effect Click -->