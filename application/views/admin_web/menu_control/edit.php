<style>
.stepwizard-step p {
    margin-top: 10px;
}

.stepwizard-row {
    display: table-row;
}

.stepwizard {
    display: table;
    width: 100%;
    position: relative;
}

.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}

.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;

}

.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}

.btn-circle {
  width: 30px;
  height: 30px;
  text-align: center;
  padding: 6px 0;
  font-size: 12px;
  line-height: 1.428571429;
  border-radius: 15px;
}
</style>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<section class="content-header">
    <h1>
        Collection Fish
        <small>Insert</small>
    </h1>
</section>
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                <div class="col-md-12">          
                    <div class="box-body">
                       
					   <div class="stepwizard">
						<div class="stepwizard-row setup-panel">
							<div class="stepwizard-step">
								<a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
								<p>ID Language</p>
							</div>
							<div class="stepwizard-step">
								<a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
								<p>Eng Language</p>
							</div>
							<div class="stepwizard-step">
								<a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
								<p>Eng Language</p>
							</div>
						</div>
					</div>
					<form role="form" action="<?php echo site_url('admin_web/menu_control/input_proses');?>" method="POST" id="input"> 
						<div class="row setup-content" id="step-1">
							<div class="col-xs-12">
								<div class="col-md-12">
									<h3> ID Language</h3>
									<input name="id_menu" maxlength="200" type="hidden" class="form-control" value="<?php echo $id_menu;?>"/>
									<div class="form-group">
										<label class="control-label">Nama</label>
										<input name="nama" maxlength="200" type="text" required="required" class="form-control" placeholder="Enter Nama" value="<?php echo $nama;?>"/>
									</div>
									<div class="form-group">
										<label class="control-label">Informasi</label>
										<textarea rows="10" name="informasi" type="text" required="required" class="form-control" placeholder="Enter Informasi" /><?php echo $informasi;?></textarea>
									</div>
									<button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
								</div>
							</div>
						</div>
						<div class="row setup-content" id="step-2">
							<div class="col-xs-12">
								<div class="col-md-12">
									<h3> ENG Language</h3>
									<div class="form-group">
										<label class="control-label">Name</label>
										<input name="name" maxlength="200" type="text" required="required" class="form-control" placeholder="Enter Name" value="<?php echo $name;?>"/>
									</div>
									<div class="form-group">
										<label class="control-label">Information</label>
										<textarea rows="10" name="information" type="text" required="required" class="form-control" placeholder="Enter Information" /><?php echo $information;?></textarea>
									</div>
									<button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
								</div>
							</div>
						</div>
						<div class="row setup-content" id="step-3">
							<div class="col-xs-12">
								<div class="col-md-12">
									<h3> ENG Language</h3>
									<button class="btn btn-success btn-lg pull-right" type="submit">SUBMIT</button>
								</div>
							</div>
						</div>
					</form>
						
                    </div><!-- /.box-body -->

                    <div class="box-footer">                 
                        <a href="<?php echo site_url('admin_web/menu_control'); ?>" class="btn btn-primary">Kembali</a>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</section><!-- /.content -->

<script type="text/javascript">
$(document).ready(function () {

    var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
                $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allNextBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;

        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-primary').trigger('click');
});
</script>