<style>
.stepwizard-step p {
    margin-top: 10px;
}

.stepwizard-row {
    display: table-row;
}

.stepwizard {
    display: table;
    width: 100%;
    position: relative;
}

.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}

.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;

}

.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}

.btn-circle {
  width: 30px;
  height: 30px;
  text-align: center;
  padding: 6px 0;
  font-size: 12px;
  line-height: 1.428571429;
  border-radius: 15px;
}
</style>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<section class="content-header">
    <h1>
        Collection Fish
        <small>Insert</small>
    </h1>
</section>
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                <div class="col-md-12">          
                    <div class="box-body">
                       
					   <div class="stepwizard">
						<div class="stepwizard-row setup-panel">
							<div class="stepwizard-step">
								<a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
								<p>General</p>
							</div>
							<div class="stepwizard-step">
								<a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
								<p>ID Language</p>
							</div>
							<div class="stepwizard-step">
								<a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
								<p>Eng Language</p>
							</div>
						</div>
					</div>
					<form role="form" action="<?php echo site_url('admin_web/fish_control/input_proses');?>" method="POST" id="input"> 
						<div class="row setup-content" id="step-1">
							<div class="col-xs-12">
								<div class="col-md-12">
									<input name="id_fish" maxlength="200" type="hidden" class="form-control" value="<?php echo $id_fish; ?>"/>
									<h3> General</h3>
									<div class="form-group">
										<label class="control-label">Icon</label>
										<input type="file" multiple="multiple" id="img_icon" name="img_icon" required="required" class="form-control"/>
										<!--<img id="preview_icon" src="<?php echo base_url('assets/img/default_img/icon_default.png');?>" alt="your image" style="width:100px"/>
										-->
										<div id="preview_icon"><img id="preview_icon" src="<?php echo base_url($avatar);?>" alt="your image" style="width:100px"/></div>
									</div>
									<div class="form-group">
										<label class="control-label">Peta Pesebaran</label>
										<input type="file" multiple="multiple" id="img_peta" name="img_peta" required="required" class="form-control"/>
										<!--<img id="preview_peta" src="<?php echo base_url('assets/img/default_img/map_default.png');?>" alt="your image" style="width:200px"/>
										-->
										<div id="preview_peta"><img id="preview_peta" src="<?php echo base_url($peta_pesebaran);?>" alt="your image" style="width:200px"/></div>
									</div>
									<div class="form-group">
										<label class="control-label">Video</label>
										<input type="file" name="video" class="form-control file_multi_video" accept="video/*" required="required"/>
										<video width="400" controls>
										  <source src="<?php echo base_url($video);?>" id="video_preview">
											Your browser does not support HTML5 video.
										</video>
									</div>
									<div class="form-group">
										<label class="control-label">Panjang Maksimal</label>
										<input name="panjang_maks" maxlength="200" type="text" required="required" class="form-control" placeholder="Enter Panjang Maks" value="<?php echo $panjang_max; ?>"/>
									</div>
									<div class="form-group">
										<label class="control-label">Class/Kelas</label>
										<input name="kelas" maxlength="200" type="text" required="required" class="form-control" placeholder="Enter Class" value="<?php echo $kelas_klasifikasi; ?>"/>
									</div>
									<div class="form-group">
										<label class="control-label">Orde/Ordo</label>
										<input name="ordo" maxlength="200" type="text" required="required" class="form-control" placeholder="Enter Orde" value="<?php echo $ordo_klasifikasi; ?>"/>
									</div>
									<div class="form-group">
										<label class="control-label">Family/Keluarga</label>
										<input name="keluarga" maxlength="200" type="text" required="required" class="form-control" placeholder="Enter Family" value="<?php echo $famili_klasifikasi; ?>"/>
									</div>
									<div class="form-group">
										<label class="control-label">Genus/Marga</label>
										<input name="marga" maxlength="200" type="text" required="required" class="form-control" placeholder="Enter Genus" value="<?php echo $marga_klasifikasi; ?>"/>
									</div>
									<div class="form-group">
										<label class="control-label">Species/Jenis</label>
										<input name="jenis" maxlength="200" type="text" required="required" class="form-control" placeholder="Enter Species" value="<?php echo $jenis_klasifikasi; ?>"/>
									</div>
									<button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
								</div>
							</div>
						</div>
						<div class="row setup-content" id="step-2">
							<div class="col-xs-12">
								<div class="col-md-12">
									<h3> ID Language</h3>
									<div class="form-group">
										<label class="control-label">Headline ID</label>
										<input type="file" multiple="multiple" id="img_headlineID" name="img_headline_id" required="required" class="form-control"/>
										<!--<img id="preview_headlineID" src="<?php echo base_url('assets/img/default_img/headline_default.png');?>" alt="your image" style="width:200px"/>
										-->
										<div id="preview_headlineID"><img id="preview_headlineID" src="<?php echo base_url($headline_id);?>" alt="your image" style="width:200px"/></div>
									</div>
									<div class="form-group">
										<label class="control-label">Nama</label>
										<input name="nama" maxlength="200" type="text" required="required" class="form-control" placeholder="Enter Nama" value="<?php echo $nama; ?>"/>
									</div>
									<div class="form-group">
										<label class="control-label">Informasi</label>
										<textarea rows="10" name="informasi" type="text" required="required" class="form-control" placeholder="Enter Informasi" /><?php echo $info_bahasa; ?></textarea>
									</div>
									<div class="form-group">
										<label class="control-label">Makanan</label>
										<input name="makanan" maxlength="200" type="text" required="required" class="form-control" placeholder="Enter Makanan" value="<?php echo $makanan; ?>"/>
									</div>
									<button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
								</div>
							</div>
						</div>
						<div class="row setup-content" id="step-3">
							<div class="col-xs-12">
								<div class="col-md-12">
									<h3> ENG Language</h3>
									<div class="form-group">
										<label class="control-label">Headline ENG</label>
										<input type="file" multiple="multiple" id="img_headlineENG" name="img_headline_eng" required="required" class="form-control"/>
										<!--<img id="preview_headlineENG" src="<?php echo base_url('assets/img/default_img/headline_default.png');?>" alt="your image" style="width:200px"/>
										-->
										<div id="preview_headlineENG"><img id="preview_headlineENG" src="<?php echo base_url($headline_english);?>" alt="your image" style="width:200px"/></div>
									</div>
									<div class="form-group">
										<label class="control-label">Name</label>
										<input name="name" maxlength="200" type="text" required="required" class="form-control" placeholder="Enter Name" value="<?php echo $name; ?>"/>
									</div>
									<div class="form-group">
										<label class="control-label">Information</label>
										<textarea rows="10" name="information" type="text" required="required" class="form-control" placeholder="Enter Information" /><?php echo $info_english; ?></textarea>
									</div>
									<div class="form-group">
										<label class="control-label">Food</label>
										<input name="food" maxlength="200" type="text" required="required" class="form-control" placeholder="Enter Food" value="<?php echo $food; ?>"/>
									</div>
									<button class="btn btn-success btn-lg pull-right" type="submit">SUBMIT</button>
								</div>
							</div>
						</div>
					</form>
						
                    </div><!-- /.box-body -->

                    <div class="box-footer">                 
                        <a href="<?php echo site_url('admin_web/fish_control'); ?>" class="btn btn-primary">Kembali</a>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</section><!-- /.content -->

<script type="text/javascript">
$(document).ready(function () {

    var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
                $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allNextBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;

        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-primary').trigger('click');
});
</script>

<script type="text/javascript">
$(document).on("change", ".file_multi_video", function(evt) {
  var $source = $('#video_preview');
  $source[0].src = URL.createObjectURL(this.files[0]);
  $source.parent()[0].load();
});
</script>

<script language="javascript" type="text/javascript">
        $(function () {
            $("#img_icon").change(function () {
                if (typeof (FileReader) != "undefined") {
                    var dvPreview = $("#preview_icon");
                    dvPreview.html("");
                    var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
                    $($(this)[0].files).each(function () {
                        var file = $(this);
                        if (regex.test(file[0].name.toLowerCase())) {
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                var img = $("<img />");
                                img.attr("style", "width: 100px; height: 100px");
                                img.attr("src", e.target.result);
                                dvPreview.append(img);
                            }
                            reader.readAsDataURL(file[0]);
                        } else {
                            alert(file[0].name + " is not a valid image file.");
                            dvPreview.html("");
                            return false;
                        }
                    });
                } else {
                    alert("This browser does not support HTML5 FileReader.");
                }
            });
        });

        $(function () {
            $("#img_peta").change(function () {
                if (typeof (FileReader) != "undefined") {
                    var dvPreview = $("#preview_peta");
                    dvPreview.html("");
                    var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
                    $($(this)[0].files).each(function () {
                        var file = $(this);
                        if (regex.test(file[0].name.toLowerCase())) {
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                var img = $("<img />");
                                img.attr("style", "width: 200px");
                                img.attr("src", e.target.result);
                                dvPreview.append(img);
                            }
                            reader.readAsDataURL(file[0]);
                        } else {
                            alert(file[0].name + " is not a valid image file.");
                            dvPreview.html("");
                            return false;
                        }
                    });
                } else {
                    alert("This browser does not support HTML5 FileReader.");
                }
            });
        });
</script>

<script language="javascript" type="text/javascript">
        $(function () {
            $("#img_headlineID").change(function () {
                if (typeof (FileReader) != "undefined") {
                    var dvPreview = $("#preview_headlineID");
                    dvPreview.html("");
                    var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
                    $($(this)[0].files).each(function () {
                        var file = $(this);
                        if (regex.test(file[0].name.toLowerCase())) {
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                var img = $("<img />");
                                img.attr("style", "width: 150px");
                                img.attr("src", e.target.result);
                                dvPreview.append(img);
                            }
                            reader.readAsDataURL(file[0]);
                        } else {
                            alert(file[0].name + " is not a valid image file.");
                            dvPreview.html("");
                            return false;
                        }
                    });
                } else {
                    alert("This browser does not support HTML5 FileReader.");
                }
            });
        });

        $(function () {
            $("#img_headlineENG").change(function () {
                if (typeof (FileReader) != "undefined") {
                    var dvPreview = $("#preview_headlineENG");
                    dvPreview.html("");
                    var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
                    $($(this)[0].files).each(function () {
                        var file = $(this);
                        if (regex.test(file[0].name.toLowerCase())) {
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                var img = $("<img />");
                                img.attr("style", "width: 150px");
                                img.attr("src", e.target.result);
                                dvPreview.append(img);
                            }
                            reader.readAsDataURL(file[0]);
                        } else {
                            alert(file[0].name + " is not a valid image file.");
                            dvPreview.html("");
                            return false;
                        }
                    });
                } else {
                    alert("This browser does not support HTML5 FileReader.");
                }
            });
        });
</script>