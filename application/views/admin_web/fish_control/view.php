<link rel="stylesheet" href="<?php echo base_url('assets/plugins/datatables/bootstrap/css/bootstrap.css') ?>"/>
<link rel="stylesheet" href="<?php echo base_url('assets/plugins/datatables/datatables/dataTables.bootstrap.css') ?>"/>

<section class="content-header">
    <h1>
        Zone Game
        <small></small>
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
			<div class='box-header with-border'>
                <h3 class='box-title'><a href="<?php echo base_url('admin_web/fish_control/insert'); ?>" class="btn btn-primary btn-small">
                <i class="glyphicon glyphicon-plus"></i> Insert Data</a></h3>
                <label calss='control-label' ></label>
            </div>
			
            <div class="box box-primary">
                <div class="box-body table-responsive">
					<table class="table table-bordered table-striped" id="mytable2">
                <thead>
                    <tr>
                        <!--
						<th><center>No</center></th>
						-->
						<th><center>No</center></th>
						<th><center>Icon</center></th>
						<th><center>Nama</center></th>
						<th><center>Headline ID</center></th>
                        <th><center>Action</center></th>
                    </tr>
                </thead>
                <?php 
                $i=1;
                  foreach($fish as $r) { 
                  ?>
                     <tr class="<?php echo $r->id_fish; ?>">
						<td><?php echo $i++; ?></td>
                        <td><center><img src="<?php echo base_url($r->avatar); ?>" class="" alt="" /></center></td>
                        <td><center><?php echo $r->nama; ?></center></td>
                        <td><center><img src="<?php echo base_url($r->headline_id); ?>" class="" alt="" /></center></td>
						<td>
                            <a href="<?php echo site_url('admin_web/fish_control/detail/'.$r->id_fish);?>" <i class="btn btn-info btn-sm glyphicon glyphicon-search" data-toggle="tooltip" title="Detail"></i></a></center>
                            <a href="<?php echo site_url('admin_web/fish_control/edit/'.$r->id_fish);?>" <i class="btn btn-info btn-sm glyphicon glyphicon-edit" data-toggle="tooltip" title="Edit"></i></a>
                            <a href="<?php echo site_url('admin_web/fish_control/delete/'.$r->id_fish);?>" <i class="btn btn-info btn-sm glyphicon glyphicon-trash hapus" data-toggle="tooltip" title="Delete"></i></a>
                        </td>
                     </tr>
                  <?php 
                  } 
                  ?>
            </table>   
				</div>
 
				</div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
</section><!-- /.content -->

<script src="<?php echo base_url('assets/plugins/datatables/js/jquery-1.11.2.min.js') ?>" ></script>
<script src="<?php echo base_url('assets/plugins/datatables/datatables/jquery.dataTables.js') ?>" ></script>
<script src="<?php echo base_url('assets/plugins/datatables/datatables/dataTables.bootstrap.js') ?>"></script>
<script type="text/javascript">
    $(function() {
        $('#mytable2').dataTable();
    });
</script>

<!--Auto Reload Every 60s-->
<!--
<script>
setTimeout(function() {
  location.reload();
}, 60000);
</script>
-->
<script>
    $(".hapus").click(function () {
        var jawab = confirm("Are you sure to delete the data?");
        if (jawab === true) {
//            kita set hapus false untuk mencegah duplicate request
            var hapus = false;
            if (!hapus) {
                hapus = true;
                $.post('zone/delete/', {id: $(this).attr('data-id')},
                function (data) {
                    alert(data);
                });
                hapus = false;
            }
        } else {
            return false;
        }
    });
</script>
