<section class="content-header">
    <h1>
        User Management
        <small>Seting User</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-suitcase"></i>Seting</a></li>
        <li class="active">User</li>
    </ol>
</section>
<section class="content">   

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class='box-header with-border'>
                    
                
				<form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Nama <?php echo form_error('nama') ?></label>
            <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama" value="<?php echo $nama; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">U Name <?php echo form_error('u_name') ?></label>
            <input type="text" class="form-control" name="u_name" id="u_name" placeholder="U Name" value="<?php echo $u_name; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">U Paswd <?php echo form_error('u_paswd') ?></label>
            <input type="text" class="form-control" name="u_paswd" id="u_paswd" placeholder="U Paswd" />
        </div>
	    <div class="form-group">
            <label for="varchar">U Foto <?php echo form_error('u_foto') ?></label>
            <input type="text" class="form-control" name="u_foto" id="u_foto" placeholder="U Foto" value="<?php echo $u_foto; ?>" />
        </div>
	    <div class="form-group">
            <label for="">Hak Akses</label>
            <select name="role" id="role" class="form-control ">
				<option value='#'>---Silahkan Pilih Hak Akses---</option>
				<option <?php if( $role =='Administrator'){echo "selected"; } ?> value='Administrator'>Administrator</option>
                            <option <?php if( $role =='Petugas Posyandu'){echo "selected"; } ?> value='Petugas Posyandu'>Petugas Posyandu</option>
                            <option <?php if( $role =='Petugas Bank Sampah'){echo "selected"; } ?> value='Petugas Bank Sampah'>Petugas Bank Sampah</option>
                            <option <?php if( $role =='Warga'){echo "selected"; } ?> value='Warga'>Warga</option>
            </select>
        </div>
	    <input type="hidden" name="u_id" value="<?php echo $u_id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('user') ?>" class="btn btn-default">Cancel</a>
	</form>
				</div>
                <div class="box-body table-responsive">
                   
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
</section><!-- /.content -->
