<section class="content-header">
    <h1>
        User Management
        <small>Seting User</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-suitcase"></i>Seting</a></li>
        <li class="active">User</li>
    </ol>
</section>
<section class="content">   

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class='box-header with-border'>
                    
                
				<table class="table table-hover">
					<tr><td>Nama</td><td><?php echo $nama; ?></td></tr>
					<tr><td>U Name</td><td><?php echo $u_name; ?></td></tr>
					<tr><td>U Paswd</td><td><?php echo $u_paswd; ?></td></tr>
					<tr><td>U Foto</td><td><?php echo $u_foto; ?></td></tr>
					<tr><td>Role</td><td><?php echo $role; ?></td></tr>
					<tr><td></td><td><a href="<?php echo site_url('user') ?>" class="btn btn-default">Cancel</a></td></tr>
				</table>
				</div>
                <div class="box-body table-responsive">
                   
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
</section><!-- /.content -->
