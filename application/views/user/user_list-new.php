<section class="content-header">
    <h1>
        User Management
        <small>Seting User</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-suitcase"></i>Seting</a></li>
        <li class="active">User</li>
    </ol>
</section>
<section class="content">   

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class='box-header with-border'>
                    <h3 class='box-title'><a href="<?php echo base_url('user/create'); ?>" class="btn btn-primary btn-small">
                            <i class="glyphicon glyphicon-plus"></i> Tambah Data</a></h3>
                            <label calss='control-label' ></label>				<!--
                <div class="col-md-14 text-right">	
					<a class="btn btn-primary btn" href="<?php echo base_url('user/word'); ?>">
					<i class="fa fa-file-word-o" aria-hidden="true" color="blue"></i> Word</a>
					<a class="btn btn-success" href="<?php echo base_url('user/excel'); ?>">
					<i class="fa fa-file-excel-o" aria-hidden="true" color="blue"></i> Excel</a>	
				</div>				-->
				
				</div>
                <div class="box-body table-responsive">
                 <table class="table table-bordered table-striped table-hover" id="mytable">
            <thead>
                <tr>
            <th width="20px">No</th>
		    <th width="70px" style="text-align:center">Nama</th>
		    <th style="text-align:center">Username</th>
		    <th style="text-align:center">Password</th>
		    <th style="text-align:center">Foto</th>
		    <th style="text-align:center">Role</th>
		    <th style="text-align:center">Action</th>
                </tr>
            </thead>
	    <tbody>
            <?php
            $start = 0;
            foreach ($user_data as $user)
            {
                ?>
                <tr>
		    <td style="text-align:center"><?php echo ++$start ?></td>
		    <td><?php echo $user->nama ?></td>
		    <td><?php echo $user->u_name ?></td>
		    <td><?php echo $user->u_paswd ?></td>
		    <td><?php echo $user->u_foto ?></td>
		    <td><?php echo $user->role ?></td>
		    <?php echo "
			<td style='text-align:center' width='75px'>" . anchor('user/read/' . $user->u_id, '<i class="glyphicon glyphicon-search" data-toggle="tooltip" title="View"></i>') .
			anchor('user/update/' . $user->u_id, '<i class="glyphicon glyphicon-edit" data-toggle="tooltip" title="Edit"> </i>') .
			anchor('user/delete/' . $user->u_id, '<i class="glyphicon glyphicon-trash" data-toggle="tooltip" title="Delete"></i>', array('onclick' => "return confirm('Data Akan di Hapus?')")) . "</td>
            </tr>";
            }
            ?>
            </tbody>
        </table>
        <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/plugins/datatables/jquery.dataTables.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/plugins/datatables/dataTables.bootstrap.js') ?>"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#mytable").dataTable();
            });
        </script>   
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
</section><!-- /.content -->
