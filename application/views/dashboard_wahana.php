<!DOCTYPE html>
<style>
	body { background-color: #30303d; color: #fff; }
	#chartdiv {
	  width: 100%;
	  height: 500px;
	}
</style>
<html>
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content" style="background-color: #675959;">
        <!-- Info boxes -->
        <div class="row">
		<div class="col-lg-12 col-xs-12">
			<div id="chartdiv"></div>												
        </div><!-- ./col -->
		
		</div>
    </section><!-- /.content -->
</html>

<script src="<?php echo base_url('assets/plugins/amcharts/amcharts.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/plugins/amcharts/serial.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/plugins/amcharts/amstock.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/plugins/amcharts/plugins/export/export.min.js'); ?>" type="text/javascript"></script>
<link href="<?php echo base_url('assets/plugins/amcharts/plugins/export/export.css')?>" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url('assets/plugins/amcharts/themes/dark.js'); ?>" type="text/javascript"></script>

<script>
	var chartData = [];
	generateChartData();

	function generateChartData() {
	  var data = <?php echo json_encode($grafik);?>;
	  
	  for (var key in data) {
		
		var date 		= data[key].date;
		var total 		= data[key].total;

		chartData.push( {
		  "date": date,
		  "value": total,
		  //"volume": b
		} );
	  }
	}

	var chart = AmCharts.makeChart( "chartdiv", {
	  "type": "stock",
	  "theme": "dark",
	  "dataSets": [ {
		"color": "#b0de09",
		"fieldMappings": [ {
		  "fromField": "value",
		  "toField": "value"
		}, {
		  "fromField": "volume",
		  "toField": "volume"
		} ],
		"dataProvider": chartData,
		"categoryField": "date",
		
	  } ],


	  "panels": [ {
		"title": "Value",
		"stockGraphs": [ {
		  "id": "g1",
		  "valueField": "value"
		} ],
		"stockLegend": {
		  "valueTextRegular": " ",
		  "markerType": "none"
		}
	  } ],

	  "chartScrollbarSettings": {
		"graph": "g1"
	  },

	  "chartCursorSettings": {
		"valueBalloonsEnabled": true,
		"graphBulletSize": 1,
		"valueLineBalloonEnabled": true,
		"valueLineEnabled": true,
		"valueLineAlpha": 0.5
	  },

	  "periodSelector": {
		"periods": [ {
		  "period": "DD",
		  "count": 10,
		  "label": "10 days"
		}, {
		  "period": "MM",
		  "count": 1,
		  "label": "1 month"
		}, {
		  "period": "YYYY",
		  "count": 1,
		  "label": "1 year"
		}, {
		  "period": "YTD",
		  "label": "YTD"
		}, {
		  "period": "MAX",
		  "label": "MAX"
		} ]
	  },

	  "panelsSettings": {
		"usePrefixes": true
	  },
	  "export": {
		"enabled": true
	  }
	} );
</script>