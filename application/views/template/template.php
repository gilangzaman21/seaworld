<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Seaworld ADMIN</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		
		<link rel="shortcut icon" href="<?php echo base_url('/assets/img/logo.png');?>">
		
        <!-- Bootstrap 3.3.2 -->
        <link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- Font Awesome Icons-->
        <link href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css" />
		
        <!-- Ionicons -->
		<link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Morris chart 
        <link href="<?php echo base_url('assets/js/plugins/morris/morris.css'); ?>" rel="stylesheet">
		-->
        <!-- jvectormap 
        <link href="<?php echo base_url('assets/js/plugins/jvectormap/jquery-jvectormap-1.2.2.css'); ?>" rel="stylesheet" type="text/css" />
		-->
        <!-- Daterange picker
        <link href="<?php echo base_url('assets/js/plugins/daterangepicker/daterangepicker-bs3.css'); ?>" rel="stylesheet" type="text/css" />
		 -->
        <!-- Datepicker -->
		<link href="<?php echo base_url('assets/js/plugins/datepicker/datepicker3.css'); ?>" rel="stylesheet" type="text/css" />
		<!-- Timepicker -->
		<!--
		<link href="<?php echo base_url('assets/js/plugins/timepicker/bootstrap-timepicker.min.css'); ?>" rel="stylesheet" type="text/css" />
		-->
		<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
        <!-- Datetimepicker -->
		<link href="<?php echo base_url('assets/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="<?php echo base_url('assets/css/AdminLTE.min.css'); ?>" rel="stylesheet" type="text/css" />
		<!-- Datatables 
		<link href="<?php echo base_url('assets/js/plugins/bootstrap.min.css'); ?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/js/plugins/dataTables.bootstrap.css'); ?>" rel="stylesheet">
        -->
        <!-- AdminLTE Skins. Choose a skin from the css/skins 
             folder instead of downloading all of them to reduce the load. -->
        <link href="<?php echo base_url('assets/css/skins/_all-skins.min.css'); ?>" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-green">
        <div class="wrapper">
            <?php echo $_header; ?>
            <!-- Left side column. contains the logo and sidebar -->
            <?php echo $_sidebar; ?>
            <!-- Right side column. Contains the navbar and content of the page -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <?php echo $_content; ?> 
            </div><!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Phase</b> 1.0
                </div>
                <strong>Copyright &copy; 2018 <a href="#">AR&Co</a>.</strong> All rights reserved.
            </footer>
        </div><!-- ./wrapper -->
        <!-- jQuery 2.1.3 -->
        <script src="<?php echo base_url('assets/js/plugins/jQuery/jQuery-2.1.3.min.js'); ?>"></script>
		<!-- jQuery 1.11.1 -->
        <script src="<?php echo base_url('assets/js/jquery-1.11.1.min.js'); ?>"></script>
		<!-- jQuery UI 
        <script src="<?php echo base_url('assets/js/plugins/jQueryUI/jquery-ui-1.10.3.js'); ?>"></script>
		-->
        <!-- Bootstrap 3.3.2 JS -->
        <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
        <!-- FastClick -->
        <script src="<?php echo base_url('assets/js/plugins/fastclick/fastclick.min.js'); ?>"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url('assets/js/AdminLTE/app.min.js'); ?>" type="text/javascript"></script>
        <!-- Sparkline 
        <script src="<?php echo base_url('assets/js/plugins/sparkline/jquery.sparkline.min.js'); ?>" type="text/javascript"></script>
		-->
        <!-- jvectormap 
        <script src="<?php echo base_url('assets/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js'); ?>" type="text/javascript"></script>
		-->
        <!-- daterangepicker -->
        <script src="<?php echo base_url('assets/js/plugins/daterangepicker/daterangepicker.js'); ?>" type="text/javascript"></script>
        <!-- timepicker -->
		<!--
		<script src="<?php echo base_url('assets/js/plugins/timepicker/bootstrap-timepicker.min.js'); ?>" type="text/javascript"></script>
		-->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
        <!-- datepicker -->
        <script src="<?php echo base_url('assets/js/plugins/datepicker/bootstrap-datepicker.js'); ?>" type="text/javascript"></script>
        <!-- iCheck 
        <script src="<?php echo base_url('assets/js/plugins/iCheck/icheck.min.js'); ?>" type="text/javascript"></script>
		-->
        <!-- SlimScroll 1.3.0 
        <script src="<?php echo base_url('assets/js/plugins/slimScroll/jquery.slimscroll.min.js'); ?>" type="text/javascript"></script>
		-->
        <!-- ChartJS 1.0.1 -->
        <script src="<?php echo base_url('assets/js/plugins/chartjs/Chart.min.js'); ?>" type="text/javascript"></script>
		<!-- Datetimepicker -->
		<script src="<?php echo base_url('assets/plugins/datetimepicker/js/bootstrap-datetimepicker.js'); ?>" type="text/javascript"></script>
		<!-- Datatables -->
		<script src="<?php echo base_url('assets/js/plugins/datatables/jquery.dataTables.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo base_url('assets/js/plugins/datatables/dataTables.bootstrap.js'); ?>" type="text/javascript"></script>
		
        <!-- AdminLTE for demo purposes 
        <script src="<?php echo base_url('assets/js/AdminLTE/demo.js'); ?>"></script>
		-->
    </body>
</html>
<script>
    $(function () {
        $('#tb-datatables').dataTable({"aoColumnDefs": [{"bSortable": false, "aTargets": [0]}]});       
        $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
    }); 
</script>