<!DOCTYPE html>
<html lang="id">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>SeaWorld</title>
  <!-- Favicon-->
    <link rel="icon" href="img/favicon.png" type="image/x-icon">
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets/img/logo.png'); ?>">

  <!-- CSS -->
	<link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet" >
	<link href="<?php echo base_url('assets/css/style_menu.css'); ?>" rel="stylesheet" >
</head>

<body onload="setInterval('displayServerTime()', 1000);">

	<audio src="<?php echo base_url('assets/img/audio/backsound.mp3'); ?>" autoplay="autoplay" loop></audio>
	
	<section id="main">
		<!--ROW HEADER & LOGO-->
		<div class="row">
				<div id="header">
					<b><h3 id="date"></h3></b>
				</div>
				<a href="<?php echo base_url("home/id/"); ?>"><input class="btn_home" type="image" src="<?php echo base_url('assets/img/32inch-btn_Home.png'); ?>" onClick="clicksound.playclip()" /></a>
				<div class="logo">
					<img src="<?php echo base_url('assets/img/logo.png'); ?>" class="logo_image" alt="" />
				</div>
		</div>
		
		<!-- ROW MAP -->
		<div class="row">
			<div class="map_image">
				<div class="theater_legend"><a class="tooltips" href="#"><img src="<?php echo base_url('assets/img/legend/btn_legenda-teater.png'); ?>" class="" alt="" /><span>Teater<br> <i>Theater</i></span></a></div>
				<div class="toilet_legend-1"><a class="tooltips" href="#"><img src="<?php echo base_url('assets/img/legend/btn_legenda-toilet.png'); ?>" class="" alt="" /><span>Toilet<br> <i>Toilet</i></span></a></div>
				<div class="toilet_legend-2"><a class="tooltips" href="#"><img src="<?php echo base_url('assets/img/legend/btn_legenda-toilet.png'); ?>" class="" alt="" /><span>Toilet<br> <i>Toilet</i></span></a></div>
				<div class="food_legend"><a class="tooltips" href="#"><img src="<?php echo base_url('assets/img/legend/btn_legenda-pusatMakanan.png'); ?>" class="" alt="" /><span>Pusat Makanan <br> <i>Food Court</i></span></a></div>
				<div class="information_legend"><a class="tooltips" href="#"><img src="<?php echo base_url('assets/img/legend/btn_legenda-informasi.png'); ?>" class="" alt="" /><span>Informasi<br> <i>Information</i></span></a></div>
				<div class="ticket_legend"><a class="tooltips" href="#"><img src="<?php echo base_url('assets/img/legend/btn_legenda-loketTiket.png'); ?>" class="" alt="" /><span>Loket Tiket<br> <i>Ticket Counter</i></span></a></div>
				<div class="locker_legend"><a class="tooltips" href="#"><img src="<?php echo base_url('assets/img/legend/btn_legenda-loker.png'); ?>" class="" alt="" /><span>Loker<br> <i>Locker</i></span></a></div>
				<div class="p3k_legend"><a class="tooltips" href="#"><img src="<?php echo base_url('assets/img/legend/btn_legenda-p3k.png'); ?>" class="" alt="" /><span>P3K<br> <i>P3K</i></span></a></div>
				<div class="babyCare_legend"><a class="tooltips" href="#"><img src="<?php echo base_url('assets/img/legend/btn_legenda-ruangMenyusui.png'); ?>" class="" alt="" /><span>Ruang Menyusui<br> <i>Baby Care</i></span></a></div>
				<div class="funDive_legend"><a class="tooltips" href="#"><img src="<?php echo base_url('assets/img/legend/btn_legenda-funDive.png'); ?>" class="" alt="" /><span>FunDive<br> <i>FunDive</i></span></a></div>
			</div>
		</div>
		
		<!-- ROW MENU -->
		<div class="row">
			<div class="menu_scroll">
				<?php
					$bahasa	= $this->uri->segment('2');
					
					$i		= 1;
					foreach ($menu as $list_menu) {
					if($list_menu->publish == "YES") {
						$link	= "aquarium/" . $bahasa . "/" . $list_menu->id_menu;
					} else {
						$link	= "menu/error";
					}
					
				?>
					<a href="<?php echo base_url($link); ?>"><input class="btn_list" id="btn_list" type="image" src="<?php echo base_url('assets/img/page/menu/btn_list.png'); ?>" onClick="clicksound.playclip()" />
					<div id="teks_menu"><h3><b><?php echo $i . ". " . $list_menu->nama; ?></b></h3></div><a>
				<?php
					$i++;
					}
				?>
			</div>
		</div>
	</section>
	
	<section id="footer">
		<a href="<?php echo base_url("menu/id/"); ?>"><input id="btn_bahasa" type="image" src="<?php echo base_url('assets/img/btn_indonesia.png'); ?>" onClick="clicksound.playclip()" /></a>
		<a href="<?php echo base_url("menu/eng/"); ?>"><input id="btn_bahasa" type="image" src="<?php echo base_url('assets/img/btn_english.png'); ?>" onClick="clicksound.playclip()" /></a>
		<h3 id="clock_down"></h3>
		<h3 class="font_bawah">START FROM HERE - THINK FISH</h3>
		<!--
		<button class="popup_feeding" id="btn_popupFeeding" onClick="clicksound.playclip()" data-toggle="modal" data-target="#popupFeeding"></button>
		-->
	</section>
</body>

</html>

	<!-- jQuery 2.1.3 -->
    <script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>	
    <!-- Bootstrap 3.3.2 JS -->
    <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>

<!-- Function DateTime -->	
<script type="text/javascript">
    //set timezone
    <?php date_default_timezone_set('Asia/Jakarta'); ?>
    //buat object date berdasarkan waktu di server
    var serverTime = new Date(<?php print date('Y, m, d, H, i, s, 0'); ?>);
    //buat object date berdasarkan waktu di client
    var clientTime = new Date();
    //hitung selisih
    var Diff = serverTime.getTime() - clientTime.getTime();    
    //fungsi displayTime yang dipanggil di bodyOnLoad dieksekusi tiap 1000ms = 1detik
    function displayServerTime(){
        //buat object date berdasarkan waktu di client
        var clientTime = new Date();
        //buat object date dengan menghitung selisih waktu client dan server
        var time = new Date(clientTime.getTime() + Diff);
        //ambil nilai jam
        var sh = time.getHours().toString();
        //ambil nilai menit
        var sm = time.getMinutes().toString();
        //ambil nilai detik
        var ss = time.getSeconds().toString();
        //tampilkan jam:menit:detik dengan menambahkan angka 0 jika angkanya cuma satu digit (0-9)
        
		var months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
		var myDays = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
		var date = new Date();
		var day = date.getDate();
		var month = date.getMonth();
		var thisDay = date.getDay(),
			thisDay = myDays[thisDay];
		var yy = date.getYear();
		var year = (yy < 1000) ? yy + 1900 : yy;
		var date_now = (thisDay + ', ' + day + ' ' + months[month] + ' ' + year);
	
	//document.getElementById("clock_up").innerHTML = (sh.length==1?"0"+sh:sh) + ":" + (sm.length==1?"0"+sm:sm);
	document.getElementById("clock_down").innerHTML = (sh.length==1?"0"+sh:sh) + ":" + (sm.length==1?"0"+sm:sm);
	document.getElementById("date").innerHTML = date_now;
	document.getElementById("date").innerHTML = date_now;
    }
</script>
<!-- END Function DateTime -->


<!-- Function Sound Effect Click -->
<script>
// Mouseover/ Click sound effect- by JavaScript Kit (www.javascriptkit.com)
// Visit JavaScript Kit at http://www.javascriptkit.com/ for full source code

//** Usage: Instantiate script by calling: var uniquevar=createsoundbite("soundfile1", "fallbackfile2", "fallebacksound3", etc)
//** Call: uniquevar.playclip() to play sound

var html5_audiotypes={ //define list of audio file extensions and their associated audio types. Add to it if your specified audio file isn't on this list:
	"mp3": "audio/mpeg",
	"mp4": "audio/mp4",
	"ogg": "audio/ogg",
	"wav": "audio/wav"
}

function createsoundbite(sound){
	var html5audio=document.createElement('audio')
	if (html5audio.canPlayType){ //check support for HTML5 audio
		for (var i=0; i<arguments.length; i++){
			var sourceel=document.createElement('source')
			sourceel.setAttribute('src', arguments[i])
			if (arguments[i].match(/\.(\w+)$/i))
				sourceel.setAttribute('type', html5_audiotypes[RegExp.$1])
			html5audio.appendChild(sourceel)
		}
		html5audio.load()
		html5audio.playclip=function(){
			html5audio.pause()
			html5audio.currentTime=0
			html5audio.play()
		}
		return html5audio
	}
	else{
		return {playclip:function(){throw new Error("Your browser doesn't support HTML5 audio unfortunately")}}
	}
}

//Initialize two sound clips with 1 fallback file each:

//var mouseoversound=createsoundbite("whistle.ogg", "whistle.mp3")
var clicksound=createsoundbite("../assets/img/audio/btn_click.mp3")
</script>
<!-- END Function Sound Effect Click -->

<!---------------------------- Popup Gallery ------------------------->
<div class="modal fade" id="popupGallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog ukuran_popup">
        <div class="modal-content popup_style">
            <div class="popup_gallery">
					<!-- Carousel -->
						<div id="myCarousel" class="carousel slide" data-ride="carousel">
						  <!-- Indicators -->
						  <ol class="carousel-indicators">
							<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
							<li data-target="#myCarousel" data-slide-to="1"></li>
							<li data-target="#myCarousel" data-slide-to="2"></li>
							<li data-target="#myCarousel" data-slide-to="3"></li>
							<li data-target="#myCarousel" data-slide-to="4"></li>
							<li data-target="#myCarousel" data-slide-to="5"></li>
						  </ol>

						  <!-- Wrapper for slides -->
						  <div class="carousel-inner">
						  <?php
							$i = 0;
							foreach($gallery as $image) {
							
								if ($i == 0) {
						  ?>
								<div class="item active">
								  <img src="<?php echo base_url($image->pic); ?>" alt="">
								</div>
						  <?php
								} else {
						  ?>
								<div class="item">
								  <img src="<?php echo base_url($image->pic); ?>" alt="">
								</div>
						  <?php
								}
								$i++;
							}
						  ?>
						  </div>
						</div>
					  <!-- /Carousel -->
				</div>
				<center>
					<input id="btn_close" type="image" src="<?php echo base_url('assets/img/btn_close.png'); ?>" onClick="clicksound.playclip()" data-dismiss="modal" aria-hidden="true" />
				</center>
		</div> 
    </div><!-- /.modal-content --> 
</div><!-- /.modal -->
<!---------------------------- END Popup Gallery ------------------------->

<!---------------------------- Popup Info ------------------------->
<div class="modal fade" id="popupInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog ukuran_popup">
        <div class="modal-content popup_style">
            <div class="popup_info">
				<b><?php echo $detail["info_bahasa"]; ?></b>
				<div class="info_makanan">
					<b><?php echo $detail["makanan"]; ?></b>
				</div>
				<div class="info_panjang">
					<b><?php echo $detail["panjang"]; ?></b>
				</div>
			</div>
				<center>
					<input id="btn_close" type="image" src="<?php echo base_url('assets/img/btn_close.png'); ?>" onClick="clicksound.playclip()" data-dismiss="modal" aria-hidden="true" />
				</center>
		</div> 
    </div><!-- /.modal-content --> 
</div><!-- /.modal -->
<!---------------------------- END Popup Info ------------------------->