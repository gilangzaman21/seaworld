<!DOCTYPE html>
<html lang="id">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>SeaWorld</title>
  <!-- Favicon-->
    <link rel="icon" href="img/favicon.png" type="image/x-icon">
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets/img/logo.png'); ?>">

  <!-- CSS -->
	<link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet" >
	<link href="<?php echo base_url('assets/css/style_collection.css'); ?>" rel="stylesheet" >
</head>

<body onload="setInterval('displayServerTime()', 1000);">

	<audio src="<?php echo base_url('assets/img/audio/backsound.mp3'); ?>" autoplay="autoplay" loop></audio>
	
	<section id="main">
		<!--ROW LOGO-->
		<div class="row">
				<a href="<?php echo base_url("menu_collection/eng/" . $detail["id_menu"]); ?>"><input id="" type="image" src="<?php echo base_url('assets/img/32inch-btn_Home.png'); ?>" onClick="clicksound.playclip()" /></a>
				<div class="logo">
					<img src="<?php echo base_url('assets/img/logo.png'); ?>" class="logo_image" alt="" />
				</div>
		</div>
		<!--ROW HEADLINE + AVATAR-->
		<div class="row" style="margin-top:15px">
				<div class="headline">
					<img src="<?php echo base_url($detail["headline_english"]); ?>" style="width:420px" alt="" />
				</div>
				<div class="avatar">
					<img src="<?php echo base_url($detail["avatar"]); ?>" class="" alt="" />
				</div>
		</div>
		<!--ROW INFO + KLASIFIKASI-->
		<div class="row" style="margin-top:15px">
				<div class="image_gallery">
					<!-- Carousel -->
						<div id="myCarousel" class="carousel slide" data-ride="carousel">
						  <!-- Indicators -->
						  <ol class="carousel-indicators">
							<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
							<li data-target="#myCarousel" data-slide-to="1"></li>
							<li data-target="#myCarousel" data-slide-to="2"></li>
							<li data-target="#myCarousel" data-slide-to="3"></li>
							<li data-target="#myCarousel" data-slide-to="4"></li>
							<li data-target="#myCarousel" data-slide-to="5"></li>
						  </ol>

						  <!-- Wrapper for slides -->
						  <div class="carousel-inner">
						  <?php
							$i = 0;
							foreach($gallery as $image) {
							
								if ($i == 0) {
						  ?>
								<div class="item active">
								  <img src="<?php echo base_url($image->pic); ?>" alt="">
								</div>
						  <?php
								} else {
						  ?>
								<div class="item">
								  <img src="<?php echo base_url($image->pic); ?>" alt="">
								</div>
						  <?php
								}
								$i++;
							}
						  ?>
						  </div>
						</div>
					  <!-- /Carousel -->
				</div>
				
				<div id="btn_zoom">
					<input type="image" id="btn_popupGalery" src="<?php echo base_url('assets/img/btn_zoom.png'); ?>" onClick="clicksound.playclip()" data-toggle="modal" data-target="#popupGallery" />
				</div>
				
				<div class="window_classification">
					<h5 id="medium_italic"><b>Class:</b><h5>
					<?php echo $detail["kelas"]; ?>
					<h5 id="medium_italic"><b>Order:</b><h5>
					<?php echo $detail["ordo"]; ?>
					<h5 id="medium_italic"><b>Family:</b><h5>
					<?php echo $detail["famili"]; ?>
					<h5 id="medium_italic"><b>Genus:</b><h5>
					<?php echo $detail["marga"]; ?>
					<h5 id="medium_italic"><b>Species:</b><h5>
					<?php echo $detail["jenis"]; ?>
				</div>
		</div>
		<!--ROW INFO + PETA + VIDEO-->
		<div class="row" style="margin-top:15px">
			<div class="info">
				<div class="scroll">
					<b><?php echo $detail["info_english"]; ?></b>
					<div class="info_diet">
						<b><?php echo $detail["food"]; ?></b>
					</div>
					<div class="info_length">
						<b><?php echo $detail["panjang"]; ?></b>
					</div>
				</div>
			</div>
			<div id="btn_zoom">
					<input type="image" src="<?php echo base_url('assets/img/btn_zoom.png'); ?>" onClick="clicksound.playclip()" data-toggle="modal" data-target="#popupInfo"/>
			</div>
			<center>
			<video width="240px" height="140px" autoplay loop muted poster style="margin-top:8px; margin-right: 10px; border-color: #428bcaa3;border-style: solid;">
				<source src="<?php echo base_url($detail["video"]); ?>" type="video/mp4" />
			</video>
			</center>
			<div class="peta">
				<img src="<?php echo base_url($detail["peta"]); ?>" class="" alt="" />
			</div>
			<div class="window_conservation">
				<center>
				<h5>Conservation Status:</h5>
				<h5><?php echo $detail["conservation_status"]; ?></h5>
				</center>
			</div>
		</div>
	</section>
	
	<section id="footer">
		<?php 
			$link	= $this->uri->segment('3');
		?>
		<a href="../id/<?php echo $link; ?>"><input id="btn_bahasa" type="image" src="<?php echo base_url('assets/img/btn_indonesia.png'); ?>" onClick="clicksound.playclip()" /></a>
		<a href="../eng/<?php echo $link; ?>"><input id="btn_bahasa" type="image" src="<?php echo base_url('assets/img/btn_english.png'); ?>" onClick="clicksound.playclip()" /></a>
		<h3 id="clock"></h3>
		<h3 id="date"></h3>
		<marquee id="feeding_schedule"><h3><b>Sea Turtle Weekday 09:30 / Weekend 15:30 Touch Pool 10:00 | 13:30 | 17:00 Otter 10:30 | 16:00 Main Aquarium 11:00 | 15:30 / Weekend 11:00 | 14:00 | 16:30 Piranha 12:30 Arapaima 13:00 Moray Eel Weekday 14:00 / Weekend 15:00 Sharkquarium 14:30<b><h3></marquee>
		<button class="popup_feeding" id="btn_popupFeeding" onClick="clicksound.playclip()" data-toggle="modal" data-target="#popupFeeding"></button>
	</section>
</body>

</html>

	<!-- jQuery 2.1.3 -->
    <script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>	
    <!-- Bootstrap 3.3.2 JS -->
    <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>

<!-- Function DateTime -->	
<script type="text/javascript">
    //set timezone
    <?php date_default_timezone_set('Asia/Jakarta'); ?>
    //buat object date berdasarkan waktu di server
    var serverTime = new Date(<?php print date('Y, m, d, H, i, s, 0'); ?>);
    //buat object date berdasarkan waktu di client
    var clientTime = new Date();
    //hitung selisih
    var Diff = serverTime.getTime() - clientTime.getTime();    
    //fungsi displayTime yang dipanggil di bodyOnLoad dieksekusi tiap 1000ms = 1detik
    function displayServerTime(){
        //buat object date berdasarkan waktu di client
        var clientTime = new Date();
        //buat object date dengan menghitung selisih waktu client dan server
        var time = new Date(clientTime.getTime() + Diff);
        //ambil nilai jam
        var sh = time.getHours().toString();
        //ambil nilai menit
        var sm = time.getMinutes().toString();
        //ambil nilai detik
        var ss = time.getSeconds().toString();
        //tampilkan jam:menit:detik dengan menambahkan angka 0 jika angkanya cuma satu digit (0-9)
        
		var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
		var myDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
		var date = new Date();
		var day = date.getDate();
		var month = date.getMonth();
		var thisDay = date.getDay(),
			thisDay = myDays[thisDay];
		var yy = date.getYear();
		var year = (yy < 1000) ? yy + 1900 : yy;
		var date_now = (months[month] + ' ' + day + ', ' + year);
	
	document.getElementById("clock").innerHTML = (sh.length==1?"0"+sh:sh) + ":" + (sm.length==1?"0"+sm:sm);
	document.getElementById("date").innerHTML = date_now;
    }
</script>
<!-- END Function DateTime -->


<!-- Function Sound Effect Click -->
<script>
// Mouseover/ Click sound effect- by JavaScript Kit (www.javascriptkit.com)
// Visit JavaScript Kit at http://www.javascriptkit.com/ for full source code

//** Usage: Instantiate script by calling: var uniquevar=createsoundbite("soundfile1", "fallbackfile2", "fallebacksound3", etc)
//** Call: uniquevar.playclip() to play sound

var html5_audiotypes={ //define list of audio file extensions and their associated audio types. Add to it if your specified audio file isn't on this list:
	"mp3": "audio/mpeg",
	"mp4": "audio/mp4",
	"ogg": "audio/ogg",
	"wav": "audio/wav"
}

function createsoundbite(sound){
	var html5audio=document.createElement('audio')
	if (html5audio.canPlayType){ //check support for HTML5 audio
		for (var i=0; i<arguments.length; i++){
			var sourceel=document.createElement('source')
			sourceel.setAttribute('src', arguments[i])
			if (arguments[i].match(/\.(\w+)$/i))
				sourceel.setAttribute('type', html5_audiotypes[RegExp.$1])
			html5audio.appendChild(sourceel)
		}
		html5audio.load()
		html5audio.playclip=function(){
			html5audio.pause()
			html5audio.currentTime=0
			html5audio.play()
		}
		return html5audio
	}
	else{
		return {playclip:function(){throw new Error("Your browser doesn't support HTML5 audio unfortunately")}}
	}
}

//Initialize two sound clips with 1 fallback file each:

//var mouseoversound=createsoundbite("whistle.ogg", "whistle.mp3")
var clicksound=createsoundbite("../../assets/img/audio/btn_click.mp3")
</script>
<!-- END Function Sound Effect Click -->

<!---------------------------- Popup Gallery ------------------------->
<div class="modal fade" id="popupGallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog ukuran_popup">
        <div class="modal-content popup_style">
            <div class="popup_gallery">
					<!-- Carousel -->
						<div id="myCarousel" class="carousel slide" data-ride="carousel">
						  <!-- Indicators -->
						  <ol class="carousel-indicators">
							<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
							<li data-target="#myCarousel" data-slide-to="1"></li>
							<li data-target="#myCarousel" data-slide-to="2"></li>
							<li data-target="#myCarousel" data-slide-to="3"></li>
							<li data-target="#myCarousel" data-slide-to="4"></li>
							<li data-target="#myCarousel" data-slide-to="5"></li>
						  </ol>

						  <!-- Wrapper for slides -->
						  <div class="carousel-inner">
						  <?php
							$i = 0;
							foreach($gallery as $image) {
							
								if ($i == 0) {
						  ?>
								<div class="item active">
								  <img src="<?php echo base_url($image->pic); ?>" alt="">
								</div>
						  <?php
								} else {
						  ?>
								<div class="item">
								  <img src="<?php echo base_url($image->pic); ?>" alt="">
								</div>
						  <?php
								}
								$i++;
							}
						  ?>
						  </div>
						</div>
					  <!-- /Carousel -->
				</div>
				<center>
					<input id="btn_close" type="image" src="<?php echo base_url('assets/img/btn_close.png'); ?>" onClick="clicksound.playclip()" data-dismiss="modal" aria-hidden="true" />
				</center>
		</div> 
    </div><!-- /.modal-content --> 
</div><!-- /.modal -->
<!---------------------------- END Popup Gallery ------------------------->

<!---------------------------- Popup Info ------------------------->
<div class="modal fade" id="popupInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog ukuran_popup">
        <div class="modal-content popup_style">
            <div class="popup_info">
				<b><?php echo $detail["info_english"]; ?></b>
				<div class="info_diet">
					<b><?php echo $detail["food"]; ?></b>
				</div>
				<div class="info_length">
					<b><?php echo $detail["panjang"]; ?></b>
				</div>
			</div>
				<center>
					<input id="btn_close" type="image" src="<?php echo base_url('assets/img/btn_close.png'); ?>" onClick="clicksound.playclip()" data-dismiss="modal" aria-hidden="true" />
				</center>
		</div> 
    </div><!-- /.modal-content --> 
</div><!-- /.modal -->
<!---------------------------- END Popup Info ------------------------->

<!---------------------------- Popup Info ------------------------->
<div class="modal fade" id="popupFeeding" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog ukuran_popup">
        <div class="modal-content popup_style">
            <div class="popup_info">
				<center>
					<img src="<?php echo base_url('assets/img/page/collection/feeding_schedule.png'); ?>" class="" alt="" />
				</center>
			</div>
				<center>
					<input id="btn_close" type="image" src="<?php echo base_url('assets/img/btn_close.png'); ?>" onClick="clicksound.playclip()" data-dismiss="modal" aria-hidden="true" />
				</center>
		</div> 
    </div><!-- /.modal-content --> 
</div><!-- /.modal -->
<!---------------------------- END Popup Info ------------------------->

<script type="text/javascript">
$(function () {
    $("#btn_popupGalery").on('click', function() {
        setTimeout(function () {
                        $("#popupGalery").modal("hide");
                    }, 5000);
    });
});
</script>