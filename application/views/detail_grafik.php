<link rel="stylesheet" href="<?php echo base_url('assets/plugins/datatables/bootstrap/css/bootstrap.css') ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('assets/plugins/datatables/datatables/dataTables.bootstrap.css') ?>"/>
<section class="content-header">
    <h1>
        Wahana Visi
        <small>Detail Grafik</small>
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body table-responsive">
					<table class="table table-bordered table-striped" id="mytable2">
                <thead>
                    <tr>
                        <th width="30px"><center>No</center></th>
						<th><center>Date</center></th>
						<th><center>Total</center></th>
                    </tr>
                </thead>
                <?php 
                $i=1;
                  foreach($grafik as $r) { 
                  ?>
                     <tr>
                        <td><?php echo $i++; ?></td>
                        <td><center><?php echo $r["date"]; ?></center></td>
						<td><center><?php echo $r["total"]; ?></center></td>
                     </tr>
                  <?php 
                  } 
                  ?>
            </table>   
				</div>
 
 
  <script src="<?php echo base_url('assets/plugins/datatables/js/jquery-1.11.2.min.js') ?>" ></script>
        <script src="<?php echo base_url('assets/plugins/datatables/datatables/jquery.dataTables.js') ?>" ></script>
        <script src="<?php echo base_url('assets/plugins/datatables/datatables/dataTables.bootstrap.js') ?>"></script>
     <script type="text/javascript">
            $(function() {
                    $('#mytable2').dataTable();

            });
        </script>
				</div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
</section><!-- /.content -->

<!--Auto Reload Every 60s-->
<script>
setTimeout(function() {
  location.reload();
}, 60000);
</script>