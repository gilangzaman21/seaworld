<link rel="stylesheet" href="<?php echo base_url('assets/plugins/datatables/bootstrap/css/bootstrap.css') ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('assets/plugins/datatables/datatables/dataTables.bootstrap.css') ?>"/>
<section class="content-header">
    <h1>
        All Player
        <small>Registered</small>
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body table-responsive">
					<table class="table table-bordered table-striped" id="mytable2">
                <thead>
                    <tr>
                        <th width="30px"><center>No</center></th>
						<th width="150px"><center>Username</center></th>
						<th width="250px"><center>Email</center></th>
						<th width="110px"><center>Birthday</center></th>
                        <th width="100px"><center>Phone</center></th>
                        <th width="190px"><center>Registration Time</center></th>
                        <th><center>Action</center></th>
                    </tr>
                </thead>
                <?php 
                $i=1;
                  foreach($player as $r) { 
                  ?>
                     <tr class="<?php echo $r->id_player; ?>">
                        <td><?php echo $i++; ?></td>
                        <td><?php echo $r->username; ?></td>
						<td><?php echo $r->email; ?></td>                        
						<td><?php echo $r->birthday; ?></td>                        
						<td><?php echo $r->phone; ?></td>                        
                        <td><?php echo $r->reg_time; ?></td>
						<td>
                            <a href="<?php echo site_url('fitur/player/detail/'.$r->id_player);?>" <i class="btn btn-info btn-sm glyphicon glyphicon-search" data-toggle="tooltip" title="Detail"></i></a></center>
                            <a href="<?php echo site_url('fitur/player/edit/'.$r->id_player);?>" <i class="btn btn-info btn-sm glyphicon glyphicon-edit" data-toggle="tooltip" title="Edit"></i></a>
                            <a href="<?php echo site_url('fitur/player/delete/'.$r->id_player);?>" <i class="btn btn-info btn-sm glyphicon glyphicon-trash" data-toggle="tooltip" title="Delete"></i></a>
                        </td>
                     </tr>
                  <?php 
                  } 
                  ?>
            </table>   
				</div>
 
 
  <script src="<?php echo base_url('assets/plugins/datatables/js/jquery-1.11.2.min.js') ?>" ></script>
        <script src="<?php echo base_url('assets/plugins/datatables/datatables/jquery.dataTables.js') ?>" ></script>
        <script src="<?php echo base_url('assets/plugins/datatables/datatables/dataTables.bootstrap.js') ?>"></script>
     <script type="text/javascript">
            $(function() {
                    $('#mytable2').dataTable();

            });
        </script>
				</div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
</section><!-- /.content -->

<!--Auto Reload Every 60s-->
<script>
setTimeout(function() {
  location.reload();
}, 60000);
</script>