<link rel="stylesheet" href="<?php echo base_url('assets/plugins/datatables/bootstrap/css/bootstrap.css') ?>"/>
<link rel="stylesheet" href="<?php echo base_url('assets/plugins/datatables/datatables/dataTables.bootstrap.css') ?>"/>

<section class="content-header">
    <h1>
        Detail Zone <?php echo $zone; ?>
        <small></small>
    </h1>
	<!--
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-suitcase"></i>Detail</a></li>
        <li class="active">Kategori Sampah</li>
    </ol>
	-->
</section>
<section class="content">   
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class='box-header with-border'>                                   
					<table class="table table-hover">
						<tr><td>Zone</td><td>: <?php echo $zone; ?></td></tr>
						<tr><td>Level Minimum</td><td>: <?php echo $min_level; ?></td></tr>
						<tr><td>Unlock Status</td><td>: <?php echo $unlock_status; ?></td></tr>
						<tr><td>Date Opened</td><td>: <?php echo $date_opened; ?></td></tr>
						<tr><td></td><td><a href="<?php echo site_url('fitur/zone') ?>" class="btn btn-default">Cancel</a></td></tr>
					</table>
				</div>
                <div class="box-body table-responsive">
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
</section><!-- /.content -->
