<style>
.btn-select {
    position: relative;
    padding: 0;
    min-width: 236px;
    width: 100%;
    border-radius: 0;
    margin-bottom: 20px;
}

.btn-select .btn-select-value {
    padding: 6px 12px;
    display: block;
    position: absolute;
    left: 0;
    right: 34px;
    text-align: left;
    text-overflow: ellipsis;
    overflow: hidden;
    border-top: none !important;
    border-bottom: none !important;
    border-left: none !important;
}

.btn-select .btn-select-arrow {
    float: right;
    line-height: 20px;
    padding: 6px 10px;
    top: 0;
}

.btn-select ul {
    display: none;
    background-color: white;
    color: black;
    clear: both;
    list-style: none;
    padding: 0;
    margin: 0;
    border-top: none !important;
    position: absolute;
    left: -1px;
    right: -1px;
    top: 33px;
    z-index: 999;
}

.btn-select ul li {
    padding: 3px 6px;
    text-align: left;
}

.btn-select ul li:hover {
    background-color: #f4f4f4;
}

.btn-select ul li.selected {
    color: white;
}

/* Default Start */
.btn-select.btn-default:hover, .btn-select.btn-default:active, .btn-select.btn-default.active {
    border-color: #ccc;
}

.btn-select.btn-default ul li.selected {
    background-color: #ccc;
}

.btn-select.btn-default ul, .btn-select.btn-default .btn-select-value {
    background-color: white;
    border: #ccc 1px solid;
}

.btn-select.btn-default:hover, .btn-select.btn-default.active {
    background-color: #e6e6e6;
}
/* Default End */

/* Primary Start */
.btn-select.btn-primary:hover, .btn-select.btn-primary:active, .btn-select.btn-primary.active {
    border-color: #286090;
}

.btn-select.btn-primary ul li.selected {
    background-color: #2e6da4;
    color: white;
}

.btn-select.btn-primary ul {
    border: #2e6da4 1px solid;
}

.btn-select.btn-primary .btn-select-value {
    background-color: #428bca;
    border: #2e6da4 1px solid;
}

.btn-select.btn-primary:hover, .btn-select.btn-primary.active {
    background-color: #286090;
}
/* Primary End */

/* Success Start */
.btn-select.btn-success:hover, .btn-select.btn-success:active, .btn-select.btn-success.active {
    border-color: #4cae4c;
}

.btn-select.btn-success ul li.selected {
    background-color: #4cae4c;
    color: white;
}

.btn-select.btn-success ul {
    border: #4cae4c 1px solid;
}

.btn-select.btn-success .btn-select-value {
    background-color: #5cb85c;
    border: #4cae4c 1px solid;
}

.btn-select.btn-success:hover, .btn-select.btn-success.active {
    background-color: #449d44;
}
/* Success End */

/* info Start */
.btn-select.btn-info:hover, .btn-select.btn-info:active, .btn-select.btn-info.active {
    border-color: #46b8da;
}

.btn-select.btn-info ul li.selected {
    background-color: #46b8da;
    color: white;
}

.btn-select.btn-info ul {
    border: #46b8da 1px solid;
}

.btn-select.btn-info .btn-select-value {
    background-color: #5bc0de;
    border: #46b8da 1px solid;
}

.btn-select.btn-info:hover, .btn-select.btn-info.active {
    background-color: #269abc;
}
/* info End */

/* warning Start */
.btn-select.btn-warning:hover, .btn-select.btn-warning:active, .btn-select.btn-warning.active {
    border-color: #eea236;
}

.btn-select.btn-warning ul li.selected {
    background-color: #eea236;
    color: white;
}

.btn-select.btn-warning ul {
    border: #eea236 1px solid;
}

.btn-select.btn-warning .btn-select-value {
    background-color: #f0ad4e;
    border: #eea236 1px solid;
}

.btn-select.btn-warning:hover, .btn-select.btn-warning.active {
    background-color: #d58512;
}
/* warning End */

/* danger Start */
.btn-select.btn-danger:hover, .btn-select.btn-danger:active, .btn-select.btn-danger.active {
    border-color: #d43f3a;
}

.btn-select.btn-danger ul li.selected {
    background-color: #d43f3a;
    color: white;
}

.btn-select.btn-danger ul {
    border: #d43f3a 1px solid;
}

.btn-select.btn-danger .btn-select-value {
    background-color: #d9534f;
    border: #d43f3a 1px solid;
}

.btn-select.btn-danger:hover, .btn-select.btn-danger.active {
    background-color: #c9302c;
}
/* danger End */

.btn-select.btn-select-light .btn-select-value {
    background-color: white;
    color: black;
}
</style>

<section class="content-header">
    <h1>
        Zone
        <small>Insert</small>
    </h1>
</section>
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                <div class="col-md-5">
                  <form role="form" action="<?php echo site_url('fitur/zone/input_proses');?>" method="POST" id="input">           
                    <div class="box-body">
                        <div class="form-group">
                            <label for="example">Zone</label>
							<input type="text" name="zone" class="form-control" required oninvalid="setCustomValidity('Zone Harus Diisi!')"
                                   oninput="setCustomValidity('')" placeholder="Masukan Zone">
                                   <?php echo form_error('zone', '<div class="text-red">', '</div>'); ?>
                        </div>                                           
                        <div class="form-group">
                            <label for="">Minimum Level Required</label>
                            <input type="text" class="form-control" name="min_level" required oninvalid="setCustomValidity('Minimum Level Harus di Isi !')"
                                   oninput="setCustomValidity('')" placeholder="Masukkan Minimum Level">
                            <?php echo form_error('min_level', '<div class="text-red">', '</div>'); ?>
                        </div>
						<div class="form-group">
                            <label for="">Unlock Status</label>
                            <a class="btn btn-info btn-select btn-select-light">
								<input type="hidden" class="btn-select-input" id="unlock_status" name="unlock_status" />
								<span class="btn-select-value">--Choose Status--</span>
								<span class='btn-select-arrow glyphicon glyphicon-chevron-down'></span>
								<ul>
									<li>YES</li>
									<li>NO</li>
								</ul>
							</a>
                        </div>			
						<div class="form-group">
                            <label for="example">Time Opened</label>
                            <div class="input-group date form_datetime" data-date-format="DD, dd MM yyyy | hh:ii" data-link-field="dtp_input1">
								<input class="form-control" type="text" name="date_opened" value="" readonly>
								<span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
								<span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
							</div>
                        </div>
						
                    </div><!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" name="submit" class="btn btn-primary"><i class="glyphicon glyphicon-hdd"></i> Simpan</button>                        
                        <a href="<?php echo site_url('fitur/zone'); ?>" class="btn btn-primary">Kembali</a>
                    </div>
                </form>
                </div>
            </div>
            </div>
        </div>
    </div>
</section><!-- /.content -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<script>
$(document).ready(function () {
    $(".btn-select").each(function (e) {
        var value = $(this).find("ul li.selected").html();
        if (value != undefined) {
            $(this).find(".btn-select-input").val(value);
            $(this).find(".btn-select-value").html(value);
        }
    });
});

$(document).on('click', '.btn-select', function (e) {
    e.preventDefault();
    var ul = $(this).find("ul");
    if ($(this).hasClass("active")) {
        if (ul.find("li").is(e.target)) {
            var target = $(e.target);
            target.addClass("selected").siblings().removeClass("selected");
            var value = target.html();
            $(this).find(".btn-select-input").val(value);
            $(this).find(".btn-select-value").html(value);
        }
        ul.hide();
        $(this).removeClass("active");
    }
    else {
        $('.btn-select').not(this).each(function () {
            $(this).removeClass("active").find("ul").hide();
        });
        ul.slideDown(300);
        $(this).addClass("active");
    }
});

$(document).on('click', function (e) {
    var target = $(e.target).closest(".btn-select");
    if (!target.length) {
        $(".btn-select").removeClass("active").find("ul").hide();
    }
});

</script>

<script type="text/javascript">
$(document).ready(function () {
	$('#timepicker1').timepicker({
		timeFormat: 'HH:mm ',
		interval: 30,
		minTime: '0',
		maxTime: '23:30',
		//defaultTime: '0',
		startTime: '00:00',
		dynamic: false,
		dropdown: true,
		scrollbar: true
	});
});
</script>

<script type="text/javascript">
$(document).ready(function () {
    $('.form_datetime').datetimepicker({
        //language:  'fr',
		format: 'yyyy-mm-dd hh:ii',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		forceParse: 0,
    });
});
</script> 