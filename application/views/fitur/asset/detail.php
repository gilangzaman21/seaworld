<section class="content-header">
    <h1>
        Kategori Sampah
        <small>Detail Kategori Sampah</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-suitcase"></i>Detail</a></li>
        <li class="active">Kategori Sampah</li>
    </ol>
</section>
<section class="content">   
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class='box-header with-border'>                                   
				<table class="table table-hover">
					<tr><td width="300">Nama Kategori Sampah</td><td>: <?php echo $nama; ?></td></tr>
					<tr><td>Harga Jual Ke Pengepul</td><td>: <?php echo $harga_jual; ?></td></tr>
					<tr><td>Harga Beli Untuk Warga</td><td>: <?php echo $harga_beli; ?></td></tr>
					<tr><td>Keterangan</td><td>: <?php echo $keterangan; ?></td></tr>
					<tr><td>Status</td><td>: <?php echo $status; ?></td></tr>
					<tr><td></td><td><a href="<?php echo site_url('fitur/player') ?>" class="btn btn-default">Cancel</a></td></tr>
				</table>
				</div>
                <div class="box-body table-responsive">
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
</section><!-- /.content -->
