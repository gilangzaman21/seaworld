<link rel="stylesheet" href="assets/plugins/sweetalert-master/alert/css/sweetalert.css">
<section class="content-header">
    <h1>
        Kategori Sampah
        <small>Edit</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-suitcase"></i>Edit</a></li>
        <li class="active">Tambah</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                <div class="col-md-5">
                  <form role="form" action="<?php echo site_url('bank_sampah/kategori/input_proses');?>" method="POST" id="input">           
                    <div class="box-body">
                        <div class="form-group">
                            <label for="example">Nama Kategori Sampah</label>
                            <input type="hidden"  name="id_kategori_sampah" value="<?php echo $id_kategori_sampah; ?>" >
							<input type="text" name="nama" class="form-control" required oninvalid="setCustomValidity('Nama Harus Diisi!')"
                                   oninput="setCustomValidity('')" placeholder="Masukan Nama Kategori Sampah" value="<?php echo $nama; ?>">
                                   <?php echo form_error('nama', '<div class="text-red">', '</div>'); ?>
                        </div>                                           
                        <div class="form-group">
                            <label for="">Harga Jual (Rp)</label>
                            <input type="text" class="form-control" name="harga_jual" required oninvalid="setCustomValidity('Harga Jual Harus di Isi !')"
                                   oninput="setCustomValidity('')" placeholder="Masukkan Harga Jual" value="<?php echo $harga_jual; ?>">
                            <?php echo form_error('merek', '<div class="text-red">', '</div>'); ?>
                        </div>
						<div class="form-group">
                            <label for="">Harga Beli (Rp)</label>
                            <input type="text" class="form-control" name="harga_beli" required oninvalid="setCustomValidity('Harga Beli Harus di Isi !')"
                                   oninput="setCustomValidity('')" placeholder="Masukkan Harga Beli" value="<?php echo $harga_beli; ?>">
                            <?php echo form_error('merek', '<div class="text-red">', '</div>'); ?>
                        </div>
						<div class="form-group">
                            <label for="">Keterangan</label>
							<textarea align="justify" class="form-control" name="keterangan"><?php echo $keterangan;?></textarea>
							
							
                        </div>  
						<div class="form-group">
                            <label for="">Status</label>
                            <select name='status' class="form-control ">
                            <option <?php if( $status=='Aktif'){echo "selected"; } ?> value='Aktif'>Aktif</option>
                            <option <?php if( $status=='Tidak Aktif'){echo "selected"; } ?> value='Tidak Aktif'>Tidak Aktif</option>
                            </select>
                        </div> 
                    </div><!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" name="submit" class="btn btn-primary"><i class="glyphicon glyphicon-hdd"></i> Simpan</button>                        
                        <a href="<?php echo site_url('bank_sampah/kategori'); ?>" class="btn btn-primary">Kembali</a>
                    </div>
                </form>
                </div>
            </div>
            </div>
        </div>
    </div>
</section><!-- /.content -->
<script src="assets/plugins/sweetalert-master/alert/js/sweetalert.min.js"></script>
<script src="assets/plugins/sweetalert-master/alert/js/qunit-1.18.0.js"></script>
<script src="assets/plugins/sweetalert-master/alert/js/jquery-2.1.4.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
  $('#input').on('submit',function(e) {  
  $.ajax({
      url:'bank_sampah/kategori/input_proses/', //nama action script php sobat
      data:$(this).serialize(),
      type:'POST',
      success:function(data){
        console.log(data);
     setTimeout(function () { 	
					swal({
						title: 'Input Success',
						text:  'Check your Data',
						type: 'success',
						timer: 2000,
						showConfirmButton: true
					});		
				},10);	
				window.setTimeout(function(){ 
					window.location.replace('bank_sampah/kategori');
				} ,2000);	
      },
      error:function(data){
     setTimeout(function () { 	
					swal({
						title: 'Failed',
						text:  'Please Try Again',
						type: 'error',
						timer: 2000,
						showConfirmButton: true
					});		
				},10);	
				window.setTimeout(function(){ 
					window.location.replace('bank_sampah/kategori');
				} ,2000);	
      }
    });
    e.preventDefault(); 
  });
});
</script>