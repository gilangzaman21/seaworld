<link rel="stylesheet" href="<?php echo base_url('assets/plugins/datatables/bootstrap/css/bootstrap.css') ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('assets/plugins/datatables/datatables/dataTables.bootstrap.css') ?>"/>
<section class="content-header">
    <h1>
        All Version
        <small>Detail Asset</small>
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body table-responsive">
					<table class="table table-bordered table-striped" id="mytable2">
                <thead>
                    <tr>
                        <th width="30px"><center>No</center></th>
						<th width="150px"><center>Version</center></th>
						<th width="230px"><center>Update Time</center></th>
						<th><center>What's New</center></th>
						<th width="100px"><center>Detail Asset</center></th>
                    </tr>
                </thead>
                <?php 
                $i=1;
                  foreach($version as $r) { 
                  ?>
                     <tr class="<?php echo $r->id_version; ?>">
                        <td><?php echo $i++; ?></td>
                        <td><center><?php echo $r->code; ?></center></td>
						<td><center><?php echo $r->update_time; ?></center></td>
						<td><?php echo $r->whats_new; ?></td>                        
						<td>
                            <center><a href="<?php echo site_url('fitur/asset/detail_asset/'.sha1($r->id_version));?>" <i class="btn btn-info btn-sm glyphicon glyphicon-search" data-toggle="tooltip" title="Detail"></i></a></center>
                        </td>
                     </tr>
                  <?php 
                  } 
                  ?>
            </table>   
				</div>
 
 
  <script src="<?php echo base_url('assets/plugins/datatables/js/jquery-1.11.2.min.js') ?>" ></script>
        <script src="<?php echo base_url('assets/plugins/datatables/datatables/jquery.dataTables.js') ?>" ></script>
        <script src="<?php echo base_url('assets/plugins/datatables/datatables/dataTables.bootstrap.js') ?>"></script>
     <script type="text/javascript">
            $(function() {
                    $('#mytable2').dataTable();

            });
        </script>
				</div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
</section><!-- /.content -->

<!--Auto Reload Every 60s-->
<script>
setTimeout(function() {
  location.reload();
}, 60000);
</script>